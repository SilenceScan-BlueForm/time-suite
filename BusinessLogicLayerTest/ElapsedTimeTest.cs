﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TimeSuite.BusinessLogicLayerTest.Utils;
using TimeSuite.SuiteLib;
using TimeSuite.SuiteLib.Globals;
using TimeSuite.SuiteLib.Interfaces.IndividualComponents;
using TimeSuite.SuiteLib.Interfaces.Models;
using TimeSuite.SuiteLib.Models;
using TimeSuite.SuiteLib.Services;

namespace TimeSuite.BusinessLogicLayerTest
{
    [TestClass]
    public class ElapsedTimeTest
    {

        private static Action<ContainerBuilder> dateTimeConfigurationCallback;

        private static Action<ContainerBuilder> DateTimeConfigurationCallback => ElapsedTimeTest.dateTimeConfigurationCallback
            ?? (ElapsedTimeTest.dateTimeConfigurationCallback = ConfigureDateTime);

        private static void ConfigureDateTime(ContainerBuilder containerBuilder) =>
            containerBuilder.RegisterInstance(DateTimeHelper).As<IDateTime>().SingleInstance();

        private static IDateTime dateTimeHelper;

        private static IDateTime DateTimeHelper => ElapsedTimeTest.dateTimeHelper
            ?? (ElapsedTimeTest.dateTimeHelper = CreateDateTimeHelper());

        private static ActivitiesServiceFactory ActivitiesServiceFactory;

        private static IDateTime CreateDateTimeHelper()
        {
            var mockDateTime = new Mock<IDateTime>();
            mockDateTime.SetupGet(dateTime => dateTime.Now).Returns(DateTime.Today.AddHours(19).AddMinutes(4).AddSeconds(1));

            return mockDateTime.Object;
        }

        [ClassInitialize]
        public static void Setup(TestContext _)
        {
            ContainerCallbacks.ConfigurationCallback += DateTimeConfigurationCallback;
            ActivitiesServiceFactory = new ActivitiesServiceFactory();
        }

        [ClassCleanup]
        public static void Teardown()
        {
            ContainerCallbacks.ConfigurationCallback -= DateTimeConfigurationCallback;
        }

        [TestMethod]
        public void TestElapsedTimeForYesterdayIfUnfinishedTimingStartedYesterday()
        {
            // Arrange
            var options = new ServiceTestingOptions
            {
                ActivityUserID = 2,
                TimingStartTimestamp = DateTime.Today.AddDays(-1).AddHours(12),
                TimingStopTimestamp = null
            };

            var activitiesService = ActivitiesServiceFactory.ArrangeServiceUnderTest(options);

            // Act
            var actualDuration = activitiesService.DayWorkedOutDuration(DateTime.Today.AddDays(-1), options.ActivityUserID);

            // Assert
            var expectedDuration = new Time(12, 0, 0, 0);
            Assert.AreEqual(0, expectedDuration.CompareTo(actualDuration));
        }

        [TestMethod]
        public void TestElapsedTimeForTodayIfUnfinishedTimingStartedYesterday()
        {
            // Arrange
            var options = new ServiceTestingOptions
            {
                ActivityUserID = 2,
                TimingStartTimestamp = DateTime.Today.AddDays(-1).AddHours(12),
                TimingStopTimestamp = null
            };

            var activitiesService = ActivitiesServiceFactory.ArrangeServiceUnderTest(options);

            // Act
            var actualDuration = activitiesService.DayWorkedOutDuration(DateTime.Today, options.ActivityUserID);

            // Assert
            var expectedDuration = new Time(19, 4, 1, 0);
            Assert.AreEqual(0, expectedDuration.CompareTo(actualDuration));
        }

        [TestMethod]
        public void TestElapsedTimeForDayBeforeYesterdayIfUnfinishedTimingStartedInDayBeforeYesterday()
        {
            // Arrange
            var options = new ServiceTestingOptions
            {
                ActivityUserID = 2,
                TimingStartTimestamp = DateTime.Today.AddDays(-2).AddHours(12),
                TimingStopTimestamp = null
            };

            var activitiesService = ActivitiesServiceFactory.ArrangeServiceUnderTest(options);

            // Act
            var actualDuration = activitiesService.DayWorkedOutDuration(DateTime.Today.AddDays(-2), options.ActivityUserID);

            // Assert
            var expectedDuration = new Time(12, 0, 0, 0);
            Assert.AreEqual(0, expectedDuration.CompareTo(actualDuration));
        }

        [TestMethod]
        public void TestElapsedTimeForYesterdayIfUnfinishedTimingStartedInDayBeforeYesterday()
        {
            // Arrange
            var options = new ServiceTestingOptions
            {
                ActivityUserID = 2,
                TimingStartTimestamp = DateTime.Today.AddDays(-2).AddHours(12),
                TimingStopTimestamp = null
            };

            var activitiesService = ActivitiesServiceFactory.ArrangeServiceUnderTest(options);

            // Act
            var actualDuration = activitiesService.DayWorkedOutDuration(DateTime.Today.AddDays(-1), options.ActivityUserID);

            // Assert
            var expectedDuration = new Time(24, 0, 0, 0);
            Assert.AreEqual(0, expectedDuration.CompareTo(actualDuration));
        }

        [TestMethod]
        public void TestElapsedTimeForTodayIfUnfinishedTimingStartedInDayBeforeYesterday()
        {
            // Arrange
            var options = new ServiceTestingOptions
            {
                ActivityUserID = 2,
                TimingStartTimestamp = DateTime.Today.AddDays(-2).AddHours(12),
                TimingStopTimestamp = null
            };

            var activitiesService = ActivitiesServiceFactory.ArrangeServiceUnderTest(options);

            // Act
            var actualDuration = activitiesService.DayWorkedOutDuration(DateTime.Today, options.ActivityUserID);

            // Assert
            var expectedDuration = new Time(19, 4, 1, 0);
            Assert.AreEqual(0, expectedDuration.CompareTo(actualDuration));
        }

        [TestMethod]
        public void TestElapsedTimeForTodayIfTimingStartedAndFinishedYesterday()
        {
            // Arrange
            var options = new ServiceTestingOptions
            {
                ActivityUserID = 2,
                TimingStartTimestamp = DateTime.Today.AddDays(-1).AddHours(8),
                TimingStopTimestamp = DateTime.Today.AddDays(-1).AddHours(16)
            };

            var activitiesService = ActivitiesServiceFactory.ArrangeServiceUnderTest(options);

            // Act
            var actualDuration = activitiesService.DayWorkedOutDuration(DateTime.Today, options.ActivityUserID);

            // Assert
            var expectedDuration = new Time(0, 0, 0, 0);
            Assert.AreEqual(0, expectedDuration.CompareTo(actualDuration));
        }

        [TestMethod]
        public void TestElapsedTimeForYesterdayIfTimingStartedAndFinishedYesterday()
        {
            // Arrange
            var options = new ServiceTestingOptions
            {
                ActivityUserID = 2,
                TimingStartTimestamp = DateTime.Today.AddDays(-1).AddHours(8),
                TimingStopTimestamp = DateTime.Today.AddDays(-1).AddHours(16)
            };

            var activitiesService = ActivitiesServiceFactory.ArrangeServiceUnderTest(options);

            // Act
            var actualDuration = activitiesService.DayWorkedOutDuration(DateTime.Today.AddDays(-1), options.ActivityUserID);

            // Assert
            var expectedDuration = new Time(8, 0, 0, 0);
            Assert.AreEqual(0, expectedDuration.CompareTo(actualDuration));
        }

        [TestMethod]
        public void TestElapsedTimeForYesterdayIfTimingStartedYesterdayStoppedToday()
        {
            // Arrange
            var options = new ServiceTestingOptions
            {
                ActivityUserID = 2,
                TimingStartTimestamp = DateTime.Today.AddDays(-1).AddHours(8),
                TimingStopTimestamp = DateTime.Today.AddHours(16)
            };

            var activitiesService = ActivitiesServiceFactory.ArrangeServiceUnderTest(options);

            // Act
            var actualDuration = activitiesService.DayWorkedOutDuration(DateTime.Today.AddDays(-1), options.ActivityUserID);

            // Assert
            var expectedDuration = new Time(16, 0, 0, 0);
            Assert.AreEqual(0, expectedDuration.CompareTo(actualDuration));
        }

        [TestMethod]
        public void TestElapsedTimeForTodayIfTimingStartedYesterdayStoppedToday()
        {
            // Arrange
            var options = new ServiceTestingOptions
            {
                ActivityUserID = 2,
                TimingStartTimestamp = DateTime.Today.AddDays(-1).AddHours(8),
                TimingStopTimestamp = DateTime.Today.AddHours(16)
            };

            var activitiesService = ActivitiesServiceFactory.ArrangeServiceUnderTest(options);

            // Act
            var actualDuration = activitiesService.DayWorkedOutDuration(DateTime.Today, options.ActivityUserID);

            // Assert
            var expectedDuration = new Time(16, 0, 0, 0);
            Assert.AreEqual(0, expectedDuration.CompareTo(actualDuration));
        }

        [TestMethod]
        public void TestElapsedTimeForTomorrowIfUnfinishedTimingStartedToday()
        {
            // Arrange
            var options = new ServiceTestingOptions
            {
                ActivityUserID = 2,
                TimingStartTimestamp = DateTime.Today.AddHours(8),
                TimingStopTimestamp = null
            };

            var activitiesService = new ActivitiesServiceFactory().ArrangeServiceUnderTest(options);

            // Act
            var actualDuration = activitiesService.DayWorkedOutDuration(DateTime.Today.AddDays(1), options.ActivityUserID);

            // Assert
            var expectedDuration = new Time(0, 0, 0, 0);
            Assert.AreEqual(0, expectedDuration.CompareTo(actualDuration));
        }
    }
}
