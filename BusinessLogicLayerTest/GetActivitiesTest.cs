﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TimeSuite.BusinessLogicLayerTest.Utils;

namespace TimeSuite.BusinessLogicLayerTest
{
    [TestClass]
    public class GetActivitiesTest
    {
        [TestMethod]
        public void TestGetByDateForTomorrow()
        {
            // Arrange
            const int currentUserID = 2;

            var options = new ServiceTestingOptions
            {
                ActivityUserID = currentUserID,
                TimingStartTimestamp = DateTime.Today.AddHours(8),
                TimingStopTimestamp = null
            };

            var activitiesService = new ActivitiesServiceFactory().ArrangeServiceUnderTest(options);

            // Act
            var activities = activitiesService.GetByDate(DateTime.Today.AddDays(1), currentUserID);

            // Assert
            Assert.AreEqual(0, activities.Count);
        }
    }
}
