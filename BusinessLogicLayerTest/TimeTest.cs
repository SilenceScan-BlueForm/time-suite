﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TimeSuite.SuiteLib;

namespace TimeSuite.BusinessLogicLayerTest
{
    [TestClass]
    public class TimeTest
    {
        [TestMethod]
        public void TestWholeDayDoesNotThrowException()
        {
            var timeSpan = new TimeSpan(days: 1, hours: 0, minutes: 0, seconds: 0);
            Time time = timeSpan;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestMoreThanWholeDayThrowsException()
        {
            var timeSpan = new TimeSpan(days: 1, hours: 0, minutes: 58, seconds: 0);
            Time time = timeSpan;
        }

        [TestMethod]
        public void TestWholeDayStringRepresentation()
        {
            var timeSpan = new TimeSpan(days: 1, hours: 0, minutes: 0, seconds: 0);
            Time time = timeSpan;

            var timeString = time.ToString();

            Assert.AreEqual("24:00:00", timeString);
        }

        [TestMethod]
        public void TestWholeDayPartsRepresentations()
        {
            var timeSpan = new TimeSpan(days: 1, hours: 0, minutes: 0, seconds: 0);
            Time time = timeSpan;

            var hours = time.Hours;
            var minutes = time.Minutes;
            var seconds = time.Seconds;
            var milliseconds = time.Milliseconds;

            Assert.AreEqual(24, hours);
            Assert.AreEqual(0, minutes);
            Assert.AreEqual(0, seconds);
            Assert.AreEqual(0, milliseconds);
        }

        [TestMethod]
        public void TestNotWholeDayStringRepresentation()
        {
            var timeSpan = new TimeSpan(days: 0, hours: 12, minutes: 6, seconds: 3, milliseconds: 300);
            Time time = timeSpan;

            var timeString = time.ToString();

            Assert.AreEqual("12:06:03", timeString);
        }

        [TestMethod]
        public void TestNotWholeDayPartsRepresentations()
        {
            var timeSpan = new TimeSpan(days: 0, hours: 12, minutes: 6, seconds: 3, milliseconds: 300);
            Time time = timeSpan;

            var hours = time.Hours;
            var minutes = time.Minutes;
            var seconds = time.Seconds;
            var milliseconds = time.Milliseconds;

            Assert.AreEqual(12, hours);
            Assert.AreEqual(6, minutes);
            Assert.AreEqual(3, seconds);
            Assert.AreEqual(300, milliseconds);
        }

        [TestMethod]
        public void TestHoursPadLeft()
        {
            var time = new Time(hours: 9, minutes: 52, seconds: 1, milliseconds: 0);
            var longString = time.ToLongString();
            Assert.AreEqual("09:52:01", longString);
        }
    }
}
