﻿using Moq;
using TimeSuite.SuiteLib.Interfaces.Models;
using TimeSuite.SuiteLib.Models;
using TimeSuite.SuiteLib.Services;

namespace TimeSuite.BusinessLogicLayerTest.Utils
{
    internal class ActivitiesServiceFactory
    {
        internal ActivitiesService ArrangeServiceUnderTest(ServiceTestingOptions options)
        {
            var mockUtil = new DbSetMockUtil();

            var activity = new Activity
            {
                ActivityID = 1,
                UserID = options.ActivityUserID
            };

            var activitiesCollection = new[] { activity };
            var activitiesSet = mockUtil.GetQueriableDbSetMock(activitiesCollection);

            var timingsCollection = new[]
            {
                new ActivityTiming
                {
                    ActivityID = activity.ActivityID,
                    Activity = activity,
                    StartTimestamp = options.TimingStartTimestamp,
                    StopTimestamp = options.TimingStopTimestamp
                }
            };

            var timingsSet = mockUtil.GetQueriableDbSetMock(timingsCollection);

            var mockContext = new Mock<ITimeSuiteContext>();
            mockContext.SetupProperty(context => context.Activities, activitiesSet);
            mockContext.SetupProperty(context => context.ActivityTimings, timingsSet);

            return new ActivitiesService(mockContext.Object);
        }
    }
}
