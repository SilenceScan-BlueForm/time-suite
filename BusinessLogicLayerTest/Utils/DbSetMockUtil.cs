﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Moq;

namespace TimeSuite.BusinessLogicLayerTest.Utils
{
    public class DbSetMockUtil
    {
        public DbSet<TEntity> GetQueriableDbSetMock<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            var queryable = entities.AsQueryable();
            var mockSet = new Mock<DbSet<TEntity>>();

            mockSet.As<IQueryable<TEntity>>().SetupGet(set => set.Provider).Returns(queryable.Provider);
            mockSet.As<IQueryable<TEntity>>().SetupGet(set => set.Expression).Returns(queryable.Expression);
            mockSet.As<IQueryable<TEntity>>().SetupGet(set => set.ElementType).Returns(queryable.ElementType);
            mockSet.As<IQueryable<TEntity>>().Setup(set => set.GetEnumerator()).Returns(() => queryable.GetEnumerator());

            return mockSet.Object;
        }
    }
}
