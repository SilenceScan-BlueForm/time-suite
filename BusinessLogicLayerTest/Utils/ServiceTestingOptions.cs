﻿using System;

namespace TimeSuite.BusinessLogicLayerTest.Utils
{
    internal class ServiceTestingOptions
        {
            public int ActivityUserID { get; set; }

            public DateTime TimingStartTimestamp { get; set; }
            public DateTime? TimingStopTimestamp { get; set; }
        }
}
