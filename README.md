# Time Suite

## Intro

The **Time Suite** is a complex software solution (`TimeSuite.sln`) intended to help you to manage your time.

It contains the **Repti** time tracker application (`TrackerAsp.csproj` for front-end, `TrackerApp.csproj` is middleware, `SuiteLib.csproj` as back-end).
The purpose of this application is to track time consumption of users and build reports on it.
It's also quite possible to use this time tracker as a simple to-do list too.
The **Repti** is implemented as an ASP .NET Web Forms project.

## Requirements

Developed and tested with:

* Windows 10 Pro v6.3
* SQL Server 2016 (SP1) v13.0.4001.0
* Visual Studio 2019 v16.1.6
* .NET Framework v4.8.03761

# Getting started

## Installing Git

This manual intended to help you to install the **Time Suite** to your machine uses Git for obtaining the sources of the solution.
Therefore, you must install the Git version control system.
Do it the way you like then check if the installation is successful before you will be able to go further.
You must know the location of a `git.exe` file.
In my case it was found in the **C:\Program Files\Git\bin\** directory.

Now, let's create the temporary alias for **git** in PowerShell.

1. Press **Win+R** on your keyboard.
1. Type `PowerShell` in the **Open** text box of the **Run** window.
1. Click the **OK** in the **Run** window.
1. Type `$git = "C:\Program Files\Git\bin\git.exe"` in the **Windows PowerShell** window.
   You may need to replace the `C:\Program Files\Git\bin\` with the actual location of the `git.exe` file in your system.

Don't close the opened PowerShell window.

## Adding an RSA key

Go to the [Access keys](https://bitbucket.org/reptisuite/time-suite/admin/access-keys/) section of the project settings
and generate then add the RSA key according to the [instructions](https://confluence.atlassian.com/bitbucket/access-keys-294486051.html).

## Obtaining the sources

In this section I will lead you through the process of downloading source codes of **Time Suite** to your computer.
Execute next commands in the previously opened PowerShell window.

1. Set the path where you want to see the source codes of the **Time Suite**: `$Src = "$Env:USERPROFILE\TimeSuite"`.
   Instead of `$Env:USERPROFILE\TimeSuite` you may use any path you want.
1. Create the directory for source codes of the **Time Suite**. For example: `mkdir "$Src" -Force`.
1. Execute the next command to change the current working directory to the directory where the sources of the **Time Suite** will sit: `cd "$Src"`
1. Set your Bitbucket user name to a `$Username` variable. For example: `$Username = "Vasily_Blinkov"`.
1. Clone the sources of the **Time Suite** using the next command: `&$git clone https://$Username@bitbucket.org/reptisuite/time-suite.git`.
   You should ignore the `NativeCommandError` in PowerShell output.

## Preparing an inegrated development environment

The **Time Suite** software complex is written using **Visual Studio Community 2017**.
If you haven't got anoter compatible version of Visual Studio, you can download
[Visual Studio Community 2017](https://visualstudio.microsoft.com/ru/vs/community/?rr=https%3A%2F%2Fduckduckgo.com%2F),
install and use it to build and deploy the **Time Suite**.

Run your installed copy of Visual Studio and be ready to further actions.

## Deploying Time Suite to file system

1. Open previously downloaded source code in Visual Studio.
   The solution should be located in `$Src\time-suite\TimeSuite.sln` file.
1. Press **Ctrl+Alt+L** on your keyboard to go to the Solution Explorer.
1. Click the **TrackerAsp** project in Solution Explorer.
1. Then, in the main menu of Visual Studio, click **Build** and **Publish TrackerAsp**.
1. In the window opened press the **Publish** button and wait a few minutes
   until the message concerning successfull publish appeared.

** NB ** This installation manual guides to through the process of installation the Time Suite to the IIS on the development machine.
         There is no script to automate the installation, but all the commands you may want to use to automate it are listed here.
		 You're able to use them to create your own customized installation script for CI/CD or other purposes.

** NB ** The `Delivery.pubxml` publish profile of the `TrackerAsp.csproj` will deploy the application to **C:\wwwroot\TimeSln1** directory.
         If you changed this setting, you will need to fix the `$PhysicalPath` variable accordingly in the **Deploy the application on IIS** section.
         
When the app will deployed, add `Full control` permissions to the containing directory for the next users:

1. `IUSR`,
1. `LOCAL SERVICE`,
1. `NETWORK SERVICE`,
1. `ReptiTimeSuite` (may be differ: this is the user name of the app pool you use to run the app).


## Run PowerShell with administrative privileges

2. Press **Ctrl+Shift+Esc** on your keyboard.
3. Click **Yes** in a **User Account Control** window.
4. In the appeared **Task Manager** window click **File**, then **Run new task**.
5. When the **Create new task** window appear, type the next command into the **Open:** text box: `PowerShell`.
6. Tick the **Create this task with administrative privileges.** checkbox in the **Create new task** window.
7. In the same **Create new task** window click the **OK** button.

## Deploy the application on IIS

Execute the next commands consistently.

** NB ** If you changed the directory **Time Suite** published to then you will need to replace the value of the `$PhysicalPath`
         variable below to the valid path of the published application location.

** NB ** If the port **85** is busy for any other application on the machine you deploying **Time Suite** to then you will need
         to replace the `85` value of the `$Port` variable below to any free port and use it instead of port **85** for
		 **Time Suite** everywhere.

```
$AppCmd = "$Env:windir\system32\inetsrv\appcmd.exe"
$ID =(&$AppCmd list site | Measure-Object).Count + 1
$PhysicalPath = "c:\wwwroot\TimeSln1"
$Site = "repti"
$Port = 85
&$AppCmd add site /name:"$Site" /id:$ID /physicalPath:"$PhysicalPath" /bindings:http/*:"$Port":
```

Do not close the **Administrator: PowerShell** window.

** Note ** If you want to access the TimeSuite instance from outside the computer it installed it, then you need to edit the
           `/configuration/appSettings/add[key="AllowedIPAddresses"]` tag's `[value]` attribute besause it's default setting
           is to restrict the access from all the computers except the localhost. Add the IP addresses of the computers you
           with to make able to access the TimeSuite instance or empty a value of the `[value]` attribute of the specified
           tag. Note, that the `IPFilter` HTTP module (in the *App_Code* directory of the *TrackerAsp* project) created to use
           `HttpContext.Request.UserHostAddress` property but it's subject to change. See the source code of the
           `BlueForm.App_Code.IPFilter` class to learn how does it work.

## Editing configurations

Execute the commands listed below in the PowerShell windows with administrative privileges opened earlier.

** NB ** No additional application pool is created using this installation manual. Instead of this it guides you to install the **Time Suite**
         to be executed under the **DefaultAppPool**.

** NB ** Some settings of the **DefaultAppPool** will be changed below.
         If this doesn't fit you, create a new application pool for the **Time Suite** and replace the `DefaultAppPool` with the name of
		 the application pool you just created. Then move the deployed application to this pool from the **DefaultAppPool**.

```
$AppPool = "DefaultAppPool"
&$AppCmd set APPPOOL $AppPool /processModel.loadUserProfile:true /processModel.setProfileEnvironment:true
$BackEndConfigRelativePath = "bin"
$BackEndConfigFilename = "SuiteLib.dll.config"
$ConfigPath = [System.IO.Path]::Combine($PhysicalPath, $BackEndConfigRelativePath, $BackEndConfigFilename)
$XMLfile = NEW-OBJECT XML
$XMLfile.Load($ConfigPath)
$ConnectionString = $XMLfile.configuration.connectionStrings.GetElementsByTagName("add") | WHERE-OBJECT -Property name -EQ -Value TimeSuite
$ConnectionString.connectionString = $ConnectionString.connectionString.Replace("MSSQLLocalDB", "sqlshared")
$XMLfile.Save($ConfigPath)
```

In newer versions of IIS you need to perform unlocking of the `/configuration/system.webserver/modules` configuration section.
To do this:

1. open the `%windir%\system32\inetsrv\config\applicationHost.config` file;
1. to the end of the file add the next node:

```xml
    <location path="ReptiTimeSuite" overrideMode="Allow">
        <system.webServer>
            <modules />
        </system.webServer>
    </location>
```

where the `ReptiTimeSuite` is the app name.

See [How to Use Locking in IIS 7.0 Configuration](https://docs.microsoft.com/en-us/iis/get-started/planning-for-security/how-to-use-locking-in-iis-configuration)
for more information.

## Database deployment

Here the simplest method of deployment of a database is described.

1. Open the **Package manager console** (**Tools**, **NuGet package manager**, **Package manager console**).
1. In the **Package manager console** pick the **SuiteLib** as the project by default.
1. Select the **Release** configuration in the **Solution Configurations** drop-down list.
   This will create the **TimeSuite.mdf** database file for "production" purpose.
1. Enter the next command `Update-Database` and press **Enter**.
1. Select the **Debug** configuration in the **Solution Configurations** drop-down list.
1. Type `Update-Database` command and press **Enter** again.
   The second call of the `Update-Database` commandlet must create a **TimeSuiteDebug.mdf** database file
   To be used for debugging purpose in the **Debug configuration**.
1. Switch back to the PowerShell window.

Then execute the next commands in the **Administrator: PowerShell** window.

```
$DataDirectory = "$PhysicalPath\App_Data"
New-Item -ItemType directory -Path $DataDirectory -Force
Copy-Item -Path TrackerAsp\App_Data\TimeSuite.mdf -Destination $DataDirectory
sqllocaldb share mssqllocaldb sqlshared
```

## Setting up permissions

**This section describes how to finish the deployment of Time Suite**

1. Open IIS manager.
1. Add all (read and modify) permissions for the created **repti** site to the **NETWORK SERVICE** account.
   To do this click on the **Delivery** node in the connections pane of IIS Manager, then click **Edit permissions...**
   in the **Actions** pane of the IIS manager. Then go to the **Secutiry**, click the **Edit...** button, then click the **Add...** button,
   enter **NETWORK SERVICE**, click **Check Names**, then **OK**, check box in cross of **Full control** and **Allow**, **OK** again.

## Possible problems and offered solutions for them

1. If the application is unable to create a database file (especially in case if you was already plaing around the Time Suite),
   try to close all the previously opened connections and remove all references to the **TimeSuite** database using a Server Explorer in Visual Studio
   or an SQL Server Management Studio. You need to to this operation switching between the debugging on IIS Express and connecting to the database
   on the full IIS at the same machine.

# Using SQL Server Express

The application was developing using LocalDB, but it is also possible to use another version of SQL Server, for example Express.
Find the actions you should perform to make the application work using SQL Server Express Edition (SSEE).

## Attach the DB

To use the SQL Server Express and existing database (reasonable when transferring the app from one server to another)
you need to _attach_ the database file to the server using SQL Server Management Studio (SSMS).
You need to attach both an MDF and an LDF (log) files.
Both of them should match each other.

Go to SSMS, login to the SSEE server.
In the Object Explorer window unfold the server node, the perform a right-click on the Databases node and pick the _Attach..._ item.
Then follow the instruction you will see on your screen.

## Permit SQL Server Express to access the database directory

1. Go to the app directory (e.g. `C:\wwwroot\TimeSln1\`)
1. Go to the data sub directory (`App_Data\`) inside the app directory
1. Perform a right click on the empty space of the `explorer` window
1. Select `Properties`
1. In the `App_Data Properties` window appeared switch to the `Security` tab
1. Click the `Edit...` button on the right of the `To change permissions, click Edit` label below the `Group or user names:` list box
1. In the `Permissions of App_Data` window appeared, click the `Add...` button below the `Group or user names:` list box
1. In the `Enter the object names to select` text box of the `Select Users or Groups` enter `NT SERVICE\MSSQL$SQLEXPRESS`
   (the user name according to the SQL Server Express may differ, refer to the
   [Configure File System Permissions for Database Engine Access](https://docs.microsoft.com/en-ca/previous-versions/sql/2014/database-engine/configure-windows/configure-file-system-permissions-for-database-engine-access?view=sql-server-2014)
   documentation article to find the whole manual of how to grant the DB engine to access the data directory of the Repti)
   and click the `Check Names` button on the right
1. Then click the `OK` button of the `Select Users or Groups` dialog to return to the `Permissoins for App_Data` window
1. In this dialog, in the `Group or user names:` list box seek and click on the `MSSQL$SQLEXPRESS` (may be different, see above) item
1. Then in the `Permissions for MSSQL$SQLEXPRESS` enable all check boxes in the `Allow` column
1. Press the `OK` button to return to the `App_Data Properties` dialog
1. Press `OK` to apply changes and close the `App_Data Properties` dialog

## Grant DB access to an app pool user

When you get the next error:
```
Cannot open database "TimeSuite" requested by the login.
The login failed. Login failed for user 'IIS APPPOOL\ReptiTimeSuite'.
```
it is the time to grant DB access permissions to the IIS application pool user.

First of all you need to add the user login.
To be able to do it, ensure that your server does support Windows authentication.

Then, in the Object Explorer window, open the SSEE server node, then the Security node and perform a right click on the Logins node.
After this select _New Login_.
In the _Login - New_ window appeared, enter the login name of the Windows login of the user used to run the app pool.
Look the error message to find it.
In my example the user login name is _IIS APPPOOL\ReptiTimeSuite_.

Then switch to the _User Mapping_ page.
In the table under the _Users mapped to this login_ label, in the row for the application DB (_TimeSuite_ in the example), set the _Map_ checkbox.

Then select the _Server Roles_ page in the navigation on the left.
Assign the _sysadmin_ role to the login to be created by setting the checkbox on the left of the _sysadmin_ item in the check box list labelled as
_Server roles_.

Click OK.

After this you need to restart the server instance.
It can be achieved by right clicking the server instance node in the Object Explorer window of SSMS and then selecting _Restart_.
You may need to do a couple of confirmations during restarting.

Then you need to restart the app pool and the app itself in the Internet Information Services Manager.
To do this, start the IIS Manager.

Then in the _Connections_ toolbar expand the node of the server where the app is running.
Click on the _Application Pools_ node.
In the working area click the pool for the app (_ReptiTimeSuite_ for example).
Then in the _Actions_ toolbar on the right click the _Recycle_ link button.

Expand the _Sites_ node in the _Connections_ toolbar.
Select the app site you want to restart (e.g. _ReptiTimeSuite_).
In the _Manage Website_ section of the _Actions_ toolbar select _Restart_.

That's all you need to do to allow the web site to access the database under your SSEE instance.
Go to your web browser and try to refresh the app page.

## Configuring Entity Framework

### Connection Factory

The application developed using Local DB so app configuration may contain the configuration of the connection factory that does not match to
your DB.
E.g. if the app configured to use Local DB by default and you use SQL Server Express Edition, Repti won't find the DB.
To cope with this situation you will need to fix the `Web.config` file.
Find the `/configuration/entityFramework/defaultConnecitonFactory` node.
Set its `[type]` attribute accordingly:

1. if you want to use Local DB `[type]` should be equal to `"System.Data.Entity.Infrastructure.LocalDbConnectionFactory, EntityFramework"`;
1. if you want to use SQL Server (including its Express edition), `[type]` should be equal to
   `"System.Data.Entity.Infrastructure.SqlConnectionFactory, EntityFramework"`.

### Server Instance Name

In the same node you may find or need to add the parameter for the connection factory specified above.
Find a couple of examples.

With Local DB, it's reasonable to set the parameter value to the instance name i.e. `"mssqllocaldb"`.

```xml
    <defaultConnectionFactory type="System.Data.Entity.Infrastructure.LocalDbConnectionFactory, EntityFramework">
      <parameters>
        <parameter value="mssqllocaldb" />
      </parameters>
    </defaultConnectionFactory>
```

When I migrated my Repti instance to SQL Server Express, I just removed the parameter and the factory finds the DB well to.

```xml
    <defaultConnectionFactory type="System.Data.Entity.Infrastructure.SqlConnectionFactory, EntityFramework">
      <!--<parameters>
        <parameter value="DESKTOP-SHCQEVL\SQLEXPRESS" />
      </parameters>-->
    </defaultConnectionFactory>
```

# The source code explained

## Configurations

### Debug

This configuration is for local debugging using Visual Studio's *Debug | Start Debugging*.
It uses the TimeSuiteDebug.mdf database located under +TimeSuite\TrackerAsp\App_Data+ folder.

Attaching the Visual Studio debugger to a _w3wp_ for debugging when this configuration is active
will cause a `SqlException` with a message like `Cannot attach the file
'...\App_Data\TimeSuiteDebug.mdf' as database 'TimeSuiteDebug'`.

This configuration is the local _development_ configuration.

### Release

This configuration if for deployment the application on the production server.
It uses the TimeSuite.mdf database from the according +App_Data+ folder with the application's
binaries used by production IIS application.

This configuration is not intended for debugging. Use the `Debug` of the `IIS` configuration
for debugging the application on IIS Express or IIS.

Change this configuration carefully because it's for the _production_ server.

The according publish configuration for this application configuration is the `Delivery`.

### IIS

This is the configuration for debugging the application deployed on IIS using Visual Studio's
*Debug | Attach to Process* (you should run the Visual Studio with administrative priviledges
and pick _w3wp_ processes for debugging).

Running the Visual Studio for debugging using F5 when this configuration is active
will cause a `SqlException` with a message like `Cannot attach the file
'...\App_Data\TimeSuiteIIS.mdf' as database 'TimeSuiteIIS'`.

This configuration is for _testing_ how the application works on Internet Information Services.

The according publish configuration for this application configuration is the `Deployment`.

# Possible problems, reasons and solutions

## SqlException: Cannot attach the file '?:\\\*\TimeSuite[\*].mdf' as database 'TimeSuite[\*]'.

*Reason:* a database with the same name but located in other directory
is already attached.

*Solution:* in SQL Server Object Explorer (in Visual Studio) or
Object Explorer (in SQL Server Management Studio) find that database and detach it
(using the Detach item in its context menu).
