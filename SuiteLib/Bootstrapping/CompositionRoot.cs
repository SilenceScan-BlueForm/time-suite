﻿using System;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using Autofac;
using Autofac.Integration.Web;
using TimeSuite.SuiteLib.Bootstrapping.IndividualComponents;
using TimeSuite.SuiteLib.Globals;
using TimeSuite.SuiteLib.Interfaces.IndividualComponents;
using TimeSuite.SuiteLib.Interfaces.Logging;
using TimeSuite.SuiteLib.Interfaces.Repositories;
using TimeSuite.SuiteLib.Managers;
using TimeSuite.SuiteLib.Repositories;

namespace TimeSuite.SuiteLib.Bootstrapping
{
    public class CompositionRoot : IContainerProviderAccessor
    {
        public ILifetimeScope LifetimeScope
        {
            get => ContainerProvider.ApplicationContainer.BeginLifetimeScope();
        }

        public IContainerProvider ContainerProvider { get; private set; }

        public CompositionRoot RegisterComponents()
        {
            var builder = new ContainerBuilder();

            this.RegisterIndividualComopnents(builder);
            this.RegisterRepositories(builder);
            this.RegisterLoggers(builder);
            this.RegisterExterns(builder);

            ContainerProvider = new ContainerProvider(builder.Build());

            return this;
        }

        private void RegisterIndividualComopnents(ContainerBuilder containerBuilder)
        {
            // TODO: Scan the IndividualComponents folder for components
            containerBuilder.Register(c => new DomainHelper())
                .AsSelf();
            containerBuilder.Register(c => new DatabaseInitializer())
                .AsSelf();
            containerBuilder.RegisterInstance(new DateTimeHelper())
                .As<IDateTime>()
                .SingleInstance();
        }

        private void RegisterRepositories(ContainerBuilder containerBuilder)
        {
            /* // TODO: Scan an assembly for components
             * builder.RegisterAssemblyTypes(myAssembly)
             *  .Where(t => t.Name.EndsWith("Repository"))
             *  .AsImplementedInterfaces();
             */
            containerBuilder.RegisterInstance(new ActivitiesRepository())
                .As<IActivitiesRepository>();
        }

        private void RegisterLoggers(ContainerBuilder containerBuilder)
        {
            /* // TODO: Scan an assembly for components
             * builder.RegisterAssemblyTypes(myAssembly)
             *  .Where(t => t.Name.EndsWith("Repository"))
             *  .AsImplementedInterfaces();
             */

            containerBuilder.Register(c => new InvalidEntitiesLogManager(DateTime.Now))
                .As<ILogger<DbEntityValidationException>>()
                .SingleInstance();

            // TODO: Use .SingleInstance(); compare mem.consumption before and after
            containerBuilder.Register(c => new MemoryLogManager(DateTime.Now))
                .As<IDisposableLogger<MemoryLogManager.WebFormName>>();

            containerBuilder.Register(c => new TelemetryLogManager())
                .As<IDisposableLogger<TelemetryLogManager.UsageInformation>>()
                .SingleInstance();

            containerBuilder.Register(c => new DbErrorsLogManager(DateTime.Now))
                .As<IDisposableLogger<DbUpdateException>>()
                .As<IDisposableLogger<DataException>>()
                .SingleInstance();
        }

        private void RegisterExterns(ContainerBuilder containerBuilder)
        {
            // TODO: This smells

            if (null == ContainerCallbacks.ConfigurationCallback)
            {
                return;
            }

            ContainerCallbacks.ConfigurationCallback(containerBuilder);
        }
    }
}
