﻿using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using Autofac;
using TimeSuite.SuiteLib.Globals;
using TimeSuite.SuiteLib.Interfaces.Logging;
using TimeSuite.SuiteLib.Managers;
using TimeSuite.SuiteLib.Models;

namespace TimeSuite.SuiteLib.Bootstrapping.IndividualComponents
{
    public class DatabaseInitializer
    {
        public void Initialize(Database db)
        {
            SetInitializer();
            InitializeDomain();
            InitializeDatabase(db);
        }

        private void SetInitializer()
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<TimeSuiteContext>());
        }

        private void InitializeDomain()
        {
            using (var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                scope.Resolve<DomainHelper>().InitializeDomain();
            }
        }

        private void InitializeDatabase(Database db)
        {
            // TODO: Debugging shown that the critical section may be required here to prevent concurrent DB creation
            // TODO: Log all unhandled exceptions using Serilog and standard unhandled exceptions technique of ASP.NET
            db.CreateIfNotExists();
        }

        public void PurveyDataIntegrity()
        {
            LogUsageInformation($"{nameof(PurveyDataIntegrity)}()");

            using (var context = new TimeSuiteContext())
                ExecuteSqlFiles(context.Database, "timing_overlaps", "current_timings", "timing_timestamps");
        }

        private void ExecuteSqlFiles(Database db, params string[] fileNamesWithoutExtension)
        {
            LogUsageInformation($"{nameof(ExecuteSqlFiles)}(Database, [{string.Join(", ", fileNamesWithoutExtension.Select(f => $"\"{f}\""))}])");

            foreach (string fileNameWithoutExtension in fileNamesWithoutExtension)
                ExecuteSqlFile(db, fileNameWithoutExtension);
        }

        private void ExecuteSqlFile(Database db, string fileNameWithoutExtension)
        {
            var signature = $"{nameof(ExecuteSqlFile)}(Database, \"{fileNameWithoutExtension}\")";
            LogUsageInformation(signature);

            var result = (object)null;
            try
            {
                result = db.ExecuteSqlCommand(File.ReadAllText(
                    HttpContext.Current.Server.MapPath(
                        Path.Combine("bin", "SQL", $"{fileNameWithoutExtension}.sql")
                    )
                ));
            }
            catch (DataException e)
            {
                result = e;
                LogDatabaseError(e);
            }

            LogUsageInformation(signature, result);
        }

        private void LogUsageInformation(string methodSignature, object result = null)
        {
            using (var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                var usageInformation = new TelemetryLogManager.UsageInformation($"{GetType().FullName}.{methodSignature}")
                {
                    Result = result
                };
                scope.Resolve<IDisposableLogger<TelemetryLogManager.UsageInformation>>().Log(usageInformation);
            }
        }

        private void LogDatabaseError(DataException e)
        {
            using (var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                var logger = scope.Resolve<IDisposableLogger<DataException>>();
                logger.Log(e);
            }
        }
    }
}
