﻿using System;
using TimeSuite.SuiteLib.Interfaces.IndividualComponents;

namespace TimeSuite.SuiteLib.Bootstrapping.IndividualComponents
{
    // TODO: Spread everywhere replacing DateTime usages
    public class DateTimeHelper : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
