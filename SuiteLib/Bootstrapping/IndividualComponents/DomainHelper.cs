﻿using System;
using System.IO;

namespace TimeSuite.SuiteLib.Bootstrapping.IndividualComponents
{
    public class DomainHelper
    {
        private const string dataDirName = "DataDirectory";

        private readonly AppDomain _currentDomain;

        public DomainHelper()
        {
            _currentDomain = AppDomain.CurrentDomain;
        }

        public void InitializeDomain()
        {
            var currentDomain = _currentDomain;
            this.InitializeDataDirectory(currentDomain);
        }

        private void InitializeDataDirectory(AppDomain domain)
        {
            // TODO: Investigate DLLs location in domains (each lib in its own domain?)
            if (string.IsNullOrWhiteSpace(DataDirectory))
            {
                var dataDirVal = Path.GetFullPath(
                   Path.Combine(domain.BaseDirectory, @"..\..\..\TrackerAsp\App_Data"));

                domain.SetData(dataDirName, dataDirVal);
            }
        }

        public string DataDirectory => _currentDomain.GetData(dataDirName) as string;
    }
}
