﻿using System;
using Autofac;

namespace TimeSuite.SuiteLib.Globals
{
    public static class ContainerCallbacks
    {
        public static Action<ContainerBuilder> ConfigurationCallback { get; set; }
    }
}
