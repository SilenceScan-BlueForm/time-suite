﻿using TimeSuite.SuiteLib.Bootstrapping;

namespace TimeSuite.SuiteLib.Globals
{
    public static class GlobalVariables
    {
        static GlobalVariables()
        {
            RegisterVariables();
        }

        public static CompositionRoot CompositionRoot { get; private set; }

        public static void RegisterVariables()
        {
            if(AreRegistered)
                return;

            CompositionRoot = new CompositionRoot().RegisterComponents();

            AreRegistered = true;
        }

        public static bool AreRegistered { get; private set; }
    }
}
