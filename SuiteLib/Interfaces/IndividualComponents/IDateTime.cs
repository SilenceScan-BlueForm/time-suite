﻿using System;

namespace TimeSuite.SuiteLib.Interfaces.IndividualComponents
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}
