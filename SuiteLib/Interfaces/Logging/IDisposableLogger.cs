﻿using System;

namespace TimeSuite.SuiteLib.Interfaces.Logging
{
    public interface IDisposableLogger<T> : ILogger<T>, IDisposable
    {
    }
}
