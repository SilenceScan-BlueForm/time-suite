﻿using TimeSuite.SuiteLib.Managers.Logs;

namespace TimeSuite.SuiteLib.Interfaces.Logging
{
    public interface IFileSinkLogger
    {
        LogFilePathGetter LogFilePathGetter { get; }
    }
}
