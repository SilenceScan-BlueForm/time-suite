﻿namespace TimeSuite.SuiteLib.Interfaces.Logging
{
    public interface ILogger<T>
    {
        void Log(T e);
    }
}
