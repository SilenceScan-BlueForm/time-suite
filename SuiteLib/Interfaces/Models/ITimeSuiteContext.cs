﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using TimeSuite.SuiteLib.Models;

namespace TimeSuite.SuiteLib.Interfaces.Models
{
    public interface ITimeSuiteContext
    {
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
        int SaveChanges();

        DbSet<Activity> Activities { get; set; }
        DbSet<ActivityTiming> ActivityTimings { get; set; }
    }
}
