﻿using System;
using System.Collections.Generic;
using TimeSuite.SuiteLib.Models;

namespace TimeSuite.SuiteLib.Interfaces.Repositories
{
    public interface IActivitiesRepository
    {
        void Create(string activityCaption, int userID);
        void FinishOne(int activityID);
        List<Activity> GetByDate(DateTime date, int userID);
        Activity GetByID(int activityID, int userID);
        Activity GetCurrent(int userID);
        List<Activity> GetUnfinished(int userID);
        int GetUnfinishedExceptCurrentActivitiesCount(int userID);
        void Resume(int activityID);
        void StopOne(int activityID);
        Time DayWorkedOutDuration(DateTime date, int userID);
        void PutActivity(int activityID, Activity editedActivity);
        ActivityTiming GetLastActivityTiming(int activityID);
        void PutActivityTiming(int activityTimingID, ActivityTiming activityTiming);
    }
}