﻿using System;
using System.Data.Entity.Validation;
using System.Text;
using Serilog;
using Serilog.Core;
using TimeSuite.SuiteLib.Interfaces.Logging;
using TimeSuite.SuiteLib.Managers.Logs;

namespace TimeSuite.SuiteLib.Managers
{
    /// <summary>
    /// Logs entities validation exceptions from the Entity Framework.
    /// </summary>
    // TODO: Should implement IDisposableLogger<DbEntityValidationException>, not ILogger<DbEntityValidationException>?
    public class InvalidEntitiesLogManager : LogManagerBase, ILogger<DbEntityValidationException>
    {
        private readonly Logger _log;

        // TODO: Do not pass timestamp here used nowhere.
        public InvalidEntitiesLogManager(DateTime timestamp) : base(timestamp)
        {
            // TODO: Log file rotation.
            var pathGetter = new LogFilePathGetter("EntitiesValidation.txt");

            _log = new LoggerConfiguration()
                .WriteTo.File(
                    pathGetter.GetLogFilePath())
                .CreateLogger();
        }

        public void Log(DbEntityValidationException e)
        {
            // TODO: Don't do this (Serilog antipattern); message must be a Serilog message template(!). Log in machine readable format.
            var message = BuildValidationMessage(e);

            // TODO: Log to Seq in the cloud (Azure) if possible.
            _log.Error(message);
        }

        private string BuildValidationMessage(DbEntityValidationException e)
        {
            var message = CreateMessageBuilder();

            message.AppendLine("Database entity validation exception");

            foreach (var eve in e.EntityValidationErrors)
            {
                message.AppendFormat("\tObject type full name: {0}\n", eve.Entry.Entity.GetType().FullName);
                message.AppendLine("\tErrors");

                foreach (var ve in eve.ValidationErrors)
                {
                    message.AppendFormat("\t\t{0}: {1}\n", ve.PropertyName, ve.ErrorMessage);
                }
            }

            return message.ToString();
        }

        private StringBuilder CreateMessageBuilder()
        {
            var message = new StringBuilder();

            message.AppendLine();

            return message;
        }
    }
}
