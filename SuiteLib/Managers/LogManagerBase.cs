﻿using System;

namespace TimeSuite.SuiteLib.Managers
{
    // TODO: Simplify creation of logging managers (see descendants' constructors).
    public abstract class LogManagerBase
    {
        protected DateTime Timestamp;

        // TODO: Remote the DateTime parameter if it is not used anywhere.
        public LogManagerBase(DateTime timestamp)
        {
            this.Timestamp = timestamp;
        }
    }
}
