﻿using Autofac;
using System.IO;
using TimeSuite.SuiteLib.Bootstrapping.IndividualComponents;
using TimeSuite.SuiteLib.Globals;

namespace TimeSuite.SuiteLib.Managers.Logs
{
    public class LogFilePathGetter
    {
        private const string _logsFolder = "Logs";
        private readonly string _filename;

        public LogFilePathGetter(string filename)
        {
            _filename = filename;
        }

        public string GetLogFilePath()
        {
            string dataDirectory;

            using (var scope = GlobalVariables.CompositionRoot.LifetimeScope)
                dataDirectory = scope.Resolve<DomainHelper>().DataDirectory;

            return Path.Combine(dataDirectory, _logsFolder, _filename);
        }
    }
}
