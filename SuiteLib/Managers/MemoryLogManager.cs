﻿using System;
using System.Diagnostics;
using Serilog;
using Serilog.Core;
using TimeSuite.SuiteLib.Interfaces.Logging;
using TimeSuite.SuiteLib.Managers.Logs;

namespace TimeSuite.SuiteLib.Managers
{
    public class MemoryLogManager : LogManagerBase,
        IDisposableLogger<MemoryLogManager.WebFormName>, IFileSinkLogger
    {
        public class WebFormName
        {
            public string PageTypeName { get; private set; }

            private WebFormName()
            {
                // Just to hide creation without specifying the required property value.
            }

            public WebFormName(string pageTypeName)
            {
                PageTypeName = pageTypeName;
            }
        }

        private struct Entity
        {
            public string FormName { get; set; }
            public long UsedBytes { get; set; }
        }

        private readonly Logger _log;

        public LogFilePathGetter LogFilePathGetter { get; private set; }

        // TODO: Are we creating instances of loggers for each log operations?!!
        public MemoryLogManager(DateTime timestamp) : base(timestamp)
        {
            LogFilePathGetter = new LogFilePathGetter("MemoryUsage.csv");
            var template =
                "{Timestamp:yyyy-MM-dd HH:mm:ss.fff},[{Level:u3}],{Message:lj}{NewLine}";

            _log = new LoggerConfiguration()
                .WriteTo.File(
                    LogFilePathGetter.GetLogFilePath(),
                    outputTemplate: template)
                .CreateLogger();
        }

        public void Log(WebFormName webFormName)
        {
            var entityToLog = GetLogEntity(webFormName.PageTypeName);

            // TODO: Don't do this (Serilog antipattern); message must be a Serilog message template(!). Log in machine readable format.
            _log.Information("{WebForm},{usedBytes}",
                entityToLog.FormName, entityToLog.UsedBytes);
        }

        private Entity GetLogEntity(string webFormName)
        {
            var allocatedPhysicalBytes = Process.GetCurrentProcess().WorkingSet64;

            return new Entity
            {
                FormName = webFormName,
                UsedBytes = allocatedPhysicalBytes
            };
        }

        public void Dispose()
        {
            _log.Dispose();
        }
    }
}
