﻿using System;
using System.Text;
using Serilog;
using Serilog.Core;
using TimeSuite.SuiteLib.Interfaces.Logging;
using TimeSuite.SuiteLib.Managers.Logs;

namespace TimeSuite.SuiteLib.Managers
{
    public class TelemetryLogManager : LogManagerBase, IDisposableLogger<TelemetryLogManager.UsageInformation>
    {
        public class UsageInformation
        {
            public string Signature { get; private set; }
            public object Result { get; set; }

            private UsageInformation()
            {
                // Just to hide creation without specifying the required property value.
            }

            public UsageInformation(string signature)
            {
                Signature = signature;
            }
        }

        private readonly Logger _log;

        public TelemetryLogManager() : base(DateTime.Now)
        {
            // TODO: Log file rotation.
            var pathGetter = new LogFilePathGetter("Telemetry.txt");

            _log = new LoggerConfiguration()
                .WriteTo.File(
                    pathGetter.GetLogFilePath())
                .CreateLogger();
        }

        public void Log(UsageInformation usageInformation)
        {
            // TODO: Don't do this (Serilog antipattern); message must be a Serilog message template(!). Log in machine readable format.
            _log.Information(BuildInformationMessage(usageInformation));
        }

        private string BuildInformationMessage(UsageInformation usageInformation)
        {
            var message = CreateMessageBuilder();

            message.AppendFormat("Method {0}", usageInformation?.Signature ?? "unknown");

            if (null != usageInformation?.Result)
            {
                message.AppendLine();
                message.AppendFormat("Result: {0}", usageInformation.Result);
            }

            return message.ToString();
        }

        private StringBuilder CreateMessageBuilder()
        {
            var message = new StringBuilder();

            message.AppendLine();

            return message;
        }

        public void Dispose()
        {
            _log.Dispose();
        }
    }
}
