namespace TimeSuite.SuiteLib.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ActivityTiming : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Activities",
                c => new
                    {
                        ActivityID = c.Int(nullable: false, identity: true),
                        ActivityName = c.String(nullable: false, maxLength: 100),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ActivityID);
            
            CreateTable(
                "dbo.ActivityTimings",
                c => new
                    {
                        TimingID = c.Int(nullable: false, identity: true),
                        ActivityID = c.Int(nullable: false),
                        StartTimestamp = c.DateTime(nullable: false),
                        StopTimestamp = c.DateTime(),
                    })
                .PrimaryKey(t => t.TimingID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ActivityTimings");
            DropTable("dbo.Activities");
        }
    }
}
