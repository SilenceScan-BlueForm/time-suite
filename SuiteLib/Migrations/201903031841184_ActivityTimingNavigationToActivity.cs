namespace TimeSuite.SuiteLib.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ActivityTimingNavigationToActivity : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.ActivityTimings", "ActivityID");
            AddForeignKey("dbo.ActivityTimings", "ActivityID", "dbo.Activities", "ActivityID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ActivityTimings", "ActivityID", "dbo.Activities");
            DropIndex("dbo.ActivityTimings", new[] { "ActivityID" });
        }
    }
}
