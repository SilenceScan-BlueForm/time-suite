namespace TimeSuite.SuiteLib.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ActivityFinished : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Activities", "Finished", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Activities", "Finished");
        }
    }
}
