namespace TimeSuite.SuiteLib.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ActivityActivityNameStringLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Activities", "ActivityName", c => c.String(nullable: false, maxLength: 200));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Activities", "ActivityName", c => c.String(nullable: false, maxLength: 100));
        }
    }
}
