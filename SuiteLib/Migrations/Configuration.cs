namespace TimeSuite.SuiteLib.Migrations
{
    using System.Data.Entity.Migrations;
    using TimeSuite.SuiteLib.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<TimeSuiteContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        // TODO: Play around deployment of the application and the database and document this process to the README.md to be used at Customers servers.
        protected override void Seed(TimeSuiteContext context)
        {
            //  This method will be called after migrating to the latest version.

            new ActivityDatabaseInitializer().Seed(context);
        }
    }
}
