﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TimeSuite.SuiteLib.Models
{
    public class Activity
    {
        public int ActivityID { get; set; }

        // TODO: Client side validation (may be not only for this property).
        [Required, StringLength(200)]
        public string ActivityName { get; set; }

        [Required]
        public int UserID { get; set; }

        public virtual ICollection<ActivityTiming> ActivityTimings { get; set; }

        [NotMapped]
        public Time ElapsedTime { get; set; }

        [Required]
        public bool Finished { get; set; } = false;
    }
}
