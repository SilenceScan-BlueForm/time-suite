﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;

namespace TimeSuite.SuiteLib.Models
{
    public class ActivityDatabaseInitializer
    {
        public void Seed(TimeSuiteContext context)
        {
            GetActivities().ForEach(a => context.Activities.AddOrUpdate(a));
            GetActivityTimings().ForEach(t => context.ActivityTimings.AddOrUpdate(t));
        }

        private static List<Activity> GetActivities()
        {
            return new List<Activity>
            {
                new Activity
                {
                    ActivityID = 1,
                    ActivityName = "Press the available blue button to change the tracking activity, green button to see the day time report, the red button to pause or to finish (complete) the current activity",
                    UserID = 1
                }
            };
        }

        private static List<ActivityTiming> GetActivityTimings()
        {
            return new List<ActivityTiming>
            {
                new ActivityTiming
                {
                    TimingID  = 1,
                    ActivityID = 1,
                    StartTimestamp = DateTime.Now,
                    StopTimestamp = null
                }
            };
        }
    }
}
