﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TimeSuite.SuiteLib.Models
{
    public class ActivityTiming : IValidatableObject
    {
        [Key]
        public int TimingID { get; set; }

        [Required]
        public int ActivityID { get; set; }
        public virtual Activity Activity { get; set; }

        [Required]
        public DateTime StartTimestamp { get; set; } = DateTime.Now;

        // TODO: Handle application error to show custom error page to a user instead of showing YSOD.
        public DateTime? StopTimestamp { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validationResults = new List<ValidationResult>();

            // Disabling this validation will lead the TimingOverlaps database function will not be able to find overlapping timings timely.
            validationResults.AddRange(ValidateThatActivityTimingNotInFuture());

            validationResults.AddRange(ValidateThatStopTimestampNotLessStartTimestamp());

            foreach (var validationResult in validationResults)
                yield return validationResult;
        }

        private IEnumerable<ValidationResult> ValidateThatActivityTimingNotInFuture()
        {
            var currentTimestamp = DateTime.Now;

            if (StartTimestamp > currentTimestamp)
            {
                yield return new ValidationResult(
                    errorMessage: $"Start timestamp ({StartTimestamp}) can't be greater than current timestamp ({currentTimestamp})",
                    memberNames: new[] { nameof(StartTimestamp) });
            }

            if ((StopTimestamp.HasValue) && (StopTimestamp > currentTimestamp))
            {
                yield return new ValidationResult(
                    errorMessage: $"Stop timestamp ({StopTimestamp}) can't be greater than current timestamp ({currentTimestamp})",
                    memberNames: new[] { nameof(StopTimestamp) });
            }
        }

        private IEnumerable<ValidationResult> ValidateThatStopTimestampNotLessStartTimestamp()
        {
            if ((StopTimestamp.HasValue) && (StartTimestamp > StopTimestamp))
            {
                yield return new ValidationResult(
                    errorMessage: $"Start timestamp ({StartTimestamp}) can't be greater than stop timestamp ({StopTimestamp})!",
                    memberNames: new[] { nameof(StartTimestamp) });
                yield return new ValidationResult(
                    errorMessage: $"Stop timestamp ({StopTimestamp}) can't be less than start timestamp ({StartTimestamp})!",
                    memberNames: new[] { nameof(StopTimestamp) });
            }
        }
    }
}
