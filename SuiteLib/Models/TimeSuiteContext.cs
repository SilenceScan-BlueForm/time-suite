﻿using Autofac;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using TimeSuite.SuiteLib.Bootstrapping.IndividualComponents;
using TimeSuite.SuiteLib.Globals;
using TimeSuite.SuiteLib.Interfaces.Logging;
using TimeSuite.SuiteLib.Interfaces.Models;

namespace TimeSuite.SuiteLib.Models
{
    public class TimeSuiteContext : DbContext, ITimeSuiteContext
    {
        private const string dbName =
#if DEBUG // Configuration: Debug
            "TimeSuiteDebug"
#elif IIS // Configuration: IIS
            "TimeSuiteIIS"
#else // Configuration: Release
            "TimeSuite"
#endif
        ;

        /// <summary>
        /// Constructor to be used for the production or debug modes.
        /// </summary>
        public TimeSuiteContext() : base(dbName)
        {
            using (var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                scope.Resolve<DatabaseInitializer>().Initialize(Database);
            }
        }

        /// <summary>
        /// Constructor to be used from the unit test project.
        /// </summary>
        /// <param name="existingConnection">
        /// Will be disposed when the context is disposed (see contextOwnsConnection).
        /// </param>
        public TimeSuiteContext(DbConnection existingConnection) : base(existingConnection, true)
        {
        }

        public DbSet<Activity> Activities { get; set; }

        public DbSet<ActivityTiming> ActivityTimings { get; set; }

        // TODO: Also override SaveChangesAsync().
        public override int SaveChanges()
        {
            int writtenEntriesNumber;

            try
            {
                writtenEntriesNumber = base.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                LogValidationErrors(e);
                throw;
            }
            catch (DbUpdateException e)
            {
                LogDbError(e);
                throw;
            }

            return writtenEntriesNumber;
        }

        private void LogValidationErrors(DbEntityValidationException e)
        {
            using (var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                scope.Resolve<ILogger<DbEntityValidationException>>().Log(e);
            }
        }

        private void LogDbError(DbUpdateException e)
        {
            using (var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                scope.Resolve<IDisposableLogger<DbUpdateException>>().Log(e);
            }
        }
    }
}
