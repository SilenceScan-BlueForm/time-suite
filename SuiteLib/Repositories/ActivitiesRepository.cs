﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TimeSuite.SuiteLib.Interfaces;
using TimeSuite.SuiteLib.Interfaces.Repositories;
using TimeSuite.SuiteLib.Models;
using TimeSuite.SuiteLib.Services;

namespace TimeSuite.SuiteLib.Repositories
{
    // TODO Extract ActivitiesTimingsRepository from the class
    public class ActivitiesRepository : IActivitiesRepository
    {
        public Activity GetCurrent(int userID)
        {
            using (var context = new TimeSuiteContext())
            {
                return new ActivitiesService(context).GetCurrent(userID);
            }
        }

        public List<Activity> GetByDate(DateTime date, int userID)
        {
            date = date.Date;

            // TODO: Performance measurements before rewriting using Autofac interception [InterceptedBy()].
            // Sample: https://stackoverflow.com/questions/32994263/autofac-interception-async-execution-order.
            // TODO: (TDD) Make one solid query for this instead of two enumerations.
            using (var context = new TimeSuiteContext())
            {
                return new ActivitiesService(context).GetByDate(date, userID);
            }
        }

        public Activity GetByID(int activityID, int userID)
        {
            using (var context = new TimeSuiteContext())
            {
                return new ActivitiesService(context).GetByID(activityID, userID);
            }
        }

        public List<Activity> GetUnfinished(int userID)
        {
            using(var context = new TimeSuiteContext())
            {
                return new ActivitiesService(context).GetUnfinished(userID);
            }
        }

        /// <returns>
        /// The count of unfinished activities for the user with the passed ID.
        /// The current activity (if present) is not count.
        /// </returns>
        public int GetUnfinishedExceptCurrentActivitiesCount(int userID)
        {
            using(var context = new TimeSuiteContext())
            {
                // TODO: What profit I will gain using the .CountAsync()?
                var count = new ActivitiesService(context).GetUnfinishedExceptCurrentActivitiesCount(userID);

                return count;
            }
        }

        public void StopOne(int activityID)
        {
            using (var context = new TimeSuiteContext())
            {
                new ActivitiesService(context).StopOne(activityID);
            }
        }

        public void FinishOne(int activityID)
        {
            using (var context = new TimeSuiteContext())
            {
                new ActivitiesService(context).FinishOne(activityID);
            }
        }

        public void Create(string activityCaption, int userID)
        {
            if (string.IsNullOrEmpty(activityCaption))
                throw new ArgumentNullException(nameof(activityCaption));

            using (var context = new TimeSuiteContext())
            {
                new ActivitiesService(context).Create(activityCaption, userID);
            }
        }

        public void Resume(int activityID)
        {
            using (var context = new TimeSuiteContext())
            {
                new ActivitiesService(context).Resume(activityID);
            }
        }

        public Time DayWorkedOutDuration(DateTime date, int userID)
        {
            using (var context = new TimeSuiteContext())
            {
                return new ActivitiesService(context).DayWorkedOutDuration(date, userID);
            }
        }

        public void PutActivity(int activityID, Activity editedActivity)
        {
            if (null == editedActivity)
                throw new ArgumentNullException(nameof(editedActivity));

            if (activityID != editedActivity.ActivityID)
                throw new ArgumentException("Wrong activity", nameof(editedActivity));

            using (var context = new TimeSuiteContext())
            {
                new ActivitiesService(context).PutActivity(activityID, editedActivity);
            }
        }

        public ActivityTiming GetLastActivityTiming(int activityID)
        {
            using (var context = new TimeSuiteContext())
            {
                return new ActivitiesService(context).GetLastActivityTiming(activityID);
            }
        }

        public void PutActivityTiming(int activityTimingID, ActivityTiming activityTiming)
        {
            if (null == activityTiming)
                throw new ArgumentNullException(nameof(activityTiming));

            if (activityTimingID != activityTiming.TimingID)
                throw new ArgumentException("Wrong timing", nameof(activityTiming));

            using (var context = new TimeSuiteContext())
            {
                new ActivitiesService(context).PutActivityTiming(activityTimingID, activityTiming);
            }
        }
    }
}
