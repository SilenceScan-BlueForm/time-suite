﻿/*
 * Create a function for getting count of current timings for a user.
 */

BEGIN TRAN;
PRINT N'Transaction started';

IF EXISTS(SELECT 1 FROM [sys].[objects] WHERE [type] = N'FN' AND [name] = N'CurrentTimings')
    RETURN;

DECLARE
    @SQL NVARCHAR(393) = N'CREATE FUNCTION CurrentTimings(
    @activity_id int
)
RETURNS int
AS
BEGIN
    DECLARE @result int;

    SELECT @result=COUNT ( 1 )
    FROM ActivityTimings T LEFT JOIN Activities A ON A.ActivityID = T.ActivityID
    WHERE A.UserID = ( SELECT TOP ( 1 ) U.UserID FROM Activities U WHERE U.ActivityID = @activity_id )
        AND T.StopTimestamp IS NULL;

    RETURN @result;
END;';

-- If the function doesn't exist then I'm going to create it.
DECLARE @exitCode int;
EXEC @exitCode = sp_executesql @SQL;

IF @exitCode = 0
BEGIN
    COMMIT TRAN;
    PRINT N'Transaction commited';
END
ELSE
BEGIN
    ROLLBACK TRAN;
    PRINT N'Transaction rolled-back';
END;
