﻿/*
 * It's the most accurate way to keep the data consistent
 */

BEGIN TRAN;
PRINT N'Transaction started';

DECLARE
    @func NVARCHAR(1613) = N' FUNCTION TimingOverlaps(
    @start_timestamp DATETIME,
    @stop_timestamp DATETIME,
    @timing_id INT
)
RETURNS BIT
AS
BEGIN
    DECLARE @overlaps_count INT;
    DECLARE @result BIT = 0;

    WITH TIMINGS AS (
        SELECT t.TimingID, t.StartTimestamp, COALESCE(t.StopTimestamp, SYSDATETIME( )) StopTimestamp
            FROM ActivityTimings t LEFT JOIN Activities a ON t.ActivityID = a.ActivityID
            WHERE
                a.UserID = (SELECT u.UserID FROM Activities u WHERE u.ActivityID = (SELECT i.ActivityID FROM ActivityTimings i WHERE i.TimingID = @timing_id))
                AND t.TimingID <> @timing_id
                AND CAST(t.StartTimestamp AS date) BETWEEN CAST(@start_timestamp AS date) AND CAST(COALESCE(@stop_timestamp, SYSDATETIME( )) AS date)
        UNION
        SELECT @timing_id TimingID, @start_timestamp StartTimestamp, COALESCE(@stop_timestamp, SYSDATETIME( )) StopTimestamp
    )
    SELECT @overlaps_count=COUNT(1) FROM TIMINGS C WHERE EXISTS(
        SELECT 1 FROM TIMINGS T WHERE
        C.TimingID <> T.TimingID AND (
        C.StartTimestamp <> C.StopTimestamp
        OR
        C.StartTimestamp/*=C.StopTimestamp*/ <> T.StartTimestamp AND C.StartTimestamp/*=C.StopTimestamp*/ <> T.StopTimestamp
        ) AND (
        ( C.StartTimestamp >= T.StartTimestamp AND C.StartTimestamp < T.StopTimestamp )
        OR
        ( T.StopTimestamp >= C.StopTimestamp AND C.StopTimestamp > T.StartTimestamp )
        )
    );

    IF @overlaps_count <> 0
        SET @result = 1;

    RETURN @result;
END;',
    @chk NVARCHAR(201) = N'ALTER TABLE [dbo].[ActivityTimings] WITH NOCHECK
    ADD CONSTRAINT [CHK_dbo.ActivityTimings_No_TimingOverlaps]
        CHECK (dbo.TimingOverlaps([StartTimestamp], [StopTimestamp], [TimingID]) <= 0);';

DECLARE @funcExists BIT
    ,@chkExists BIT;

IF EXISTS(SELECT 1 FROM [sys].[objects] WHERE [type] = N'FN' AND [name] = N'TimingOverlaps')
    SET @funcExists = 1;
ELSE
    SET @funcExists = 0;

DECLARE @SQL NVARCHAR(1618);

DECLARE @retCode INT
    ,@exitCode INT = 0;

IF EXISTS(SELECT 1 FROM [sys].[check_constraints] WHERE [name] = N'CHK_dbo.ActivityTimings_No_TimingOverlaps')
    SET @chkExists = 1;
ELSE
    SET @chkExists = 0;

/* If the function already exists I need to compare it's existing declaration to the desired one. */
IF @funcExists = 1
BEGIN
    SET @SQL = N'CREATE' + @func;

    -- 'To-be' (desired) function text.
    DECLARE @funcFutureDef TABLE( [Row#] INT, [Text] NVARCHAR(255) );
    WITH CTE AS (
        SELECT 1 [Row#], TXT.[value] [Text] FROM STRING_SPLIT ( @SQL, CHAR(10) ) TXT
    ) INSERT INTO @funcFutureDef ( [Row#], [Text] ) ( SELECT ROW_NUMBER ( ) OVER ( ORDER BY CTE.[Row#] ) [Row#], CTE.[Text] FROM CTE );

    -- 'As-is' function text.
    DECLARE @funcExistingDef TABLE( [Row#] INT, [Text] NVARCHAR(255) );
    INSERT INTO @funcExistingDef ( [Text] ) EXEC sp_helptext N'dbo.TimingOverlaps';
    WITH CTE AS (
        SELECT 1 [Row#], TXT.[Text] FROM @funcExistingDef TXT
    ) INSERT INTO @funcExistingDef ( [Row#], [Text] ) ( SELECT ROW_NUMBER ( ) OVER ( ORDER BY CTE.[Row#] ), CTE.[Text] FROM CTE );
    DELETE FROM @funcExistingDef WHERE [Row#] IS NULL;

    /* If counts of lines in the desired and the existing functions' defenitions are same
       and all rows of the desired and the existing function defenitions are same
       then the script should not touch the database. */
    IF ( SELECT COUNT ( 1 ) FROM @funcFutureDef ) = ( SELECT COUNT ( 1 ) FROM @funcExistingDef )
    BEGIN
        IF NOT EXISTS(SELECT 1 FROM @funcFutureDef FFD
            WHERE REPLACE ( REPLACE ( FFD.[Text], CHAR(13), N'' ), CHAR(10), N'' ) <>
            ( SELECT REPLACE ( REPLACE ( FED.[Text], CHAR(13), N'' ), CHAR(10), N'' ) FROM @funcExistingDef FED WHERE FED.[Row#] = FFD.[Row#] ))
        BEGIN
            IF @chkExists = 0
            BEGIN
                PRINT N'The required constraint does not exist, creating';
                EXEC @retCode = sp_executesql @chk;
                SET @exitCode += @retCode;

            END;

            PRINT N'dbo.TimingOverlaps() is up to date, exiting';

            IF @exitCode = 0
            BEGIN
                COMMIT TRAN;
                PRINT N'Transaction commited';
            END
            ELSE
            BEGIN
                ROLLBACK TRAN;
                PRINT N'Transaction rolled-back';
            END;

            RETURN;
        END;
    END;
END;

PRINT N'dbo.TimingOverlaps() does not exist or existing defenition differs from the actual one, creating or updating (including the constraint)';

-- I need to remove the constraint to be able to alter the function used in it.
IF @chkExists = 1
BEGIN
    PRINT N'Dropping a constraint to update the function used in it';
    ALTER TABLE [dbo].[ActivityTimings]
        DROP CONSTRAINT [CHK_dbo.ActivityTimings_No_TimingOverlaps];
END;

-- Create or alter the function if it doesn't exist or obsolete.
DECLARE @verb NVARCHAR(6);
IF @funcExists = 0
    SET @verb = N'CREATE'
ELSE
    SET @verb = N'ALTER';
SET @SQL = @verb + @func;
PRINT @verb + N' dbo.TimingOverlaps()';
EXEC @retCode = sp_executesql @SQL;
SET @exitCode += @retCode;

-- Re-create the constraint.
PRINT N'Creating/updating the required constraint';
EXEC @retCode = sp_executesql @chk;
SET @exitCode += @retCode;

IF @exitCode = 0
BEGIN
    COMMIT TRAN;
    PRINT N'Transaction commited';
END
ELSE
BEGIN
    ROLLBACK TRAN;
    PRINT N'Transaction rolled-back';
END;
