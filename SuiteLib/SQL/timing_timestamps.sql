﻿/*
Control that the StartTimestamp is not greater than the current timestamp was here,
but then moved to the ActivityTiming class' Validate(ValidationContext) method.
*/
IF EXISTS(SELECT 1
    FROM [sys].[check_constraints]
    WHERE [name] = 'CHK_dbo.ActivityTimings_StartTimestamp_CurrentTimestamp')
    ALTER TABLE [dbo].[ActivityTimings]
        DROP CONSTRAINT [CHK_dbo.ActivityTimings_StartTimestamp_CurrentTimestamp]

/*
Control that the StopTimestamp is not greater than the current timestamp was here,
but then moved to the ActivityTiming class' Validate(ValidationContext) method.
*/
IF EXISTS(SELECT 1
    FROM [sys].[check_constraints]
    WHERE [name] = 'CHK_dbo.ActivityTimings_StopTimestamp_CurrentTimestamp')
    ALTER TABLE [dbo].[ActivityTimings]
        DROP CONSTRAINT [CHK_dbo.ActivityTimings_StopTimestamp_CurrentTimestamp]

/*
ALTER TABLE [dbo].[ActivityTimings]
    DROP CONSTRAINT [CHK_dbo.ActivityTimings_StopTimestamp_Null]
*/
IF NOT EXISTS(SELECT 1
    FROM [sys].[check_constraints]
    WHERE [name] = 'CHK_dbo.ActivityTimings_StopTimestamp_Null')
    ALTER TABLE [dbo].[ActivityTimings] WITH NOCHECK
        ADD CONSTRAINT [CHK_dbo.ActivityTimings_StopTimestamp_Null] CHECK (dbo.CurrentTimings([ActivityID]) <= 1)
