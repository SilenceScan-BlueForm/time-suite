﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Autofac;
using TimeSuite.SuiteLib.Globals;
using TimeSuite.SuiteLib.Interfaces.IndividualComponents;
using TimeSuite.SuiteLib.Interfaces.Models;
using TimeSuite.SuiteLib.Interfaces.Repositories;
using TimeSuite.SuiteLib.Models;

namespace TimeSuite.SuiteLib.Services
{
    public class ActivitiesService : IActivitiesRepository
    {
        private readonly ITimeSuiteContext context;

        public ActivitiesService(ITimeSuiteContext context)
        {
            this.context = context;
        }

        public void Create(string activityCaption, int userID)
        {
            var activity = context.Activities.Add(new Activity
            {
                ActivityName = activityCaption,
                UserID = userID
            });

            context.SaveChanges();

            context.ActivityTimings.Add(new ActivityTiming
            {
                ActivityID = activity.ActivityID,
            });

            context.SaveChanges();
        }

        /// <returns>Elapsed time in milliseconds</returns>
        public Time DayWorkedOutDuration(DateTime date, int userID)
        {
            DateTime now;
            
            using (var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                now = scope.Resolve<IDateTime>().Now;
            }

            var today = now.Date;
            var day = date.Date;

            if (day > today)
            {
                return new TimeSpan();
            }

            int? ticked = context.ActivityTimings
                .Where(t =>
                    t.Activity.UserID == userID
                    && (
                        TestableDbFunctions.TruncateTime(t.StartTimestamp) == day
                        || (!t.StopTimestamp.HasValue && TestableDbFunctions.TruncateTime(t.StartTimestamp) <= day)
                        || TestableDbFunctions.TruncateTime(t.StopTimestamp.Value) == day
                ))
                .Sum(t => TestableDbFunctions.DiffMilliseconds(
                    TestableDbFunctions.TruncateTime(t.StartTimestamp) != day
                        ? day
                        : t.StartTimestamp,
                    day == (TestableDbFunctions.TruncateTime(t.StopTimestamp) ?? today)
                        ? t.StopTimestamp ?? now
                        : TestableDbFunctions.AddDays(day, 1)
                ));

            return TimeSpan.FromMilliseconds(ticked.GetValueOrDefault(0));
        }

        public void FinishOne(int activityID)
        {
            Activity activity = context.Activities
                .FirstOrDefault(a => activityID == a.ActivityID);

            activity.Finished = true;

            context.SaveChanges();
        }

        public List<Activity> GetByDate(DateTime date, int userID)
        {
            DateTime now;

            using (var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                now = scope.Resolve<IDateTime>().Now;
            }

            DateTime today = now.Date;

            if (date > today)
            {
                return new List<Activity>();
            }

            // TODO: Pay time zones in attention during this update query.
            var timings = context.ActivityTimings
                .Where(t =>
                    // Belonging to a requested user.
                    userID == t.Activity.UserID && (
                        // Started in a requested day.
                        date == TestableDbFunctions.TruncateTime(t.StartTimestamp) ||
                        // Or completed in a requested day.
                        t.StopTimestamp.HasValue && date == TestableDbFunctions.TruncateTime(t.StopTimestamp.Value) ||
                        // Or started before a requested day but not completed in a requested day.
                        TestableDbFunctions.TruncateTime(t.StartTimestamp) < date && !t.StopTimestamp.HasValue
                    )
                )
                .GroupBy(t => t.Activity)
                .ToList();

            List<Activity> activities = timings
                .Select(g => new Activity
                {
                    ActivityID = g.Key.ActivityID,
                    ActivityName = g.Key.ActivityName,
                    Finished = g.Key.Finished,
                    UserID = g.Key.UserID,
                    ElapsedTime = TimeSpan.FromMilliseconds(g.Sum(t =>
                        (t.StopTimestamp ?? (today == date ? now : date.AddDays(1))).Subtract(
                            t.StartTimestamp < date ? date : t.StartTimestamp
                        ).TotalMilliseconds
                    ))
                })
                .ToList();

            return activities;
        }

        public Activity GetByID(int activityID, int userID)
        {
            return context.Activities.FirstOrDefault(a => a.ActivityID == activityID && a.UserID == userID);
        }

        public Activity GetCurrent(int userID)
        {
            // TODO: Handle situations when there are several started activities for a user in DB.
            return context.ActivityTimings
                .Where(t => userID == t.Activity.UserID && !t.StopTimestamp.HasValue)
                .OrderByDescending(t => t.StartTimestamp)
                .FirstOrDefault()?.Activity;
        }

        public ActivityTiming GetLastActivityTiming(int activityID)
        {
            return context.ActivityTimings
                .Where(t => t.ActivityID == activityID)
                .OrderByDescending(t => t.StartTimestamp)
                .FirstOrDefault();
        }

        public List<Activity> GetUnfinished(int userID)
        {
            return context.Activities
                .Where(a => userID == a.UserID && !a.Finished)
                .ToList();
        }

        public int GetUnfinishedExceptCurrentActivitiesCount(int userID)
        {
            var currentActivityID = this.GetCurrent(userID)?.ActivityID;

            return context.Activities
                .Where(a =>
                    // User comparison.
                    userID == a.UserID &&
                    // Status check.
                    !a.Finished &&
                    // Ignore the current activity if present.
                    (!currentActivityID.HasValue || currentActivityID.Value != a.ActivityID))
                .Count();
        }

        public void PutActivity(int activityID, Activity editedActivity)
        {
            var activity = this.GetByID(activityID, editedActivity.UserID);

            if (null == activity)
                throw new ArgumentException($"No activity with ID={activityID} found");

            /* Updating using the context.Entry().State = EntityState.Modified
                * approach is not suitable here because of the unoptimal SQL query
                * generated by the EF: it will UPDATE each column in the row. But
                * as you can see here there is only one column should be updated.
                * See https://stackoverflow.com/questions/30987806/dbset-attachentity-vs-dbcontext-entryentity-state-entitystate-modified#30988176
                */
            activity.ActivityName = editedActivity.ActivityName;

            context.SaveChanges();
        }

        public void PutActivityTiming(int activityTimingID, ActivityTiming activityTiming)
        {
            context.Entry(activityTiming).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void Resume(int activityID)
        {
            var timing = new ActivityTiming
            {
                ActivityID = activityID,
            };

            context.ActivityTimings.Add(timing);

            context.SaveChanges();
        }

        public void StopOne(int activityID)
        {
            ActivityTiming currentTiming = context.ActivityTimings
                .Where(t => activityID == t.ActivityID && null == t.StopTimestamp)
                .OrderByDescending(t => t.StartTimestamp)
                .FirstOrDefault();
                
            if (currentTiming == null)
            {
                // TODO: Treat this case as an error and handle it properly.
                return;
            }

            // TODO: Pay time zones in attention during this update query (whole file).
            currentTiming.StopTimestamp = DateTime.Now;

            // TODO: https://docs.microsoft.com/en-us/ef/ef6/fundamentals/async (whole file).
            context.SaveChanges();
        }
    }
}
