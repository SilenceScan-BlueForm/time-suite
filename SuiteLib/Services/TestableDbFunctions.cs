﻿using System;
using System.Data.Entity;

namespace TimeSuite.SuiteLib.Services
{
    public static class TestableDbFunctions
    {
        [DbFunction("Edm", "CreateDateTime")]
        public static DateTime? CreateDateTime(int? year, int? month, int? day, int? hour, int? minute, double? second)
        {
            var fractionalSecond = second ?? 0;
            var fullSecond = (int)fractionalSecond;
            var millisecond = (int)(1E+3 * (fractionalSecond - fullSecond));

            return new DateTime(year ?? 0, month ?? 0, day ?? 0, hour ?? 0, minute ?? 0, fullSecond, millisecond);
        }

        [DbFunction("Edm", "DiffMilliseconds")]
        public static int? DiffMilliseconds(DateTime? timeValue1, DateTime? timeValue2)
        {
            return timeValue1.HasValue && timeValue2.HasValue
                ? (int?)(timeValue2.Value - timeValue1.Value).TotalMilliseconds
                : null;
        }

        [DbFunction("Edm", "AddDays")]
        public static DateTime? AddDays(DateTime? dateValue, int? addValue)
        {
            return dateValue.HasValue && addValue.HasValue
                ? dateValue.Value.AddDays(addValue.Value)
                : null as DateTime?;
        }

        [DbFunction("Edm", "TruncateTime")]
        public static DateTime? TruncateTime(DateTime? dateValue)
        {
            return dateValue.HasValue ? dateValue.Value.Date : null as DateTime?;
        }
    }
}
