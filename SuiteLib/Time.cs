﻿using System;

namespace TimeSuite.SuiteLib
{
    /// <summary>
    /// <see cref="TimeSpan"/> wrapper with advanced stringification algorithm.
    /// </summary>
    public class Time : IComparable<Time>
    {
        private TimeSpan timeSpan;

        public Time(int hours, int minutes, int seconds, int milliseconds)
        {
            var timeSpan = new TimeSpan(days: 0, hours, minutes, seconds, milliseconds);
            CheckTime(timeSpan);
            this.timeSpan = timeSpan;
        }

        private Time(TimeSpan timeSpan)
        {
            CheckTime(timeSpan);
            this.timeSpan = timeSpan;
        }

        private void CheckTime(TimeSpan timeSpan)
        {
            if(timeSpan.TotalDays > 1)
            {
                throw new ArgumentOutOfRangeException(
                    nameof(timeSpan),
                    $"Instances of the {nameof(Time)} class can hold only time within a day (24 hours)"
                );
            }
        }

        public override string ToString() => ToShortString();

        public string ToShortString()
        {
            var format = new System.Text.StringBuilder();

            if (this.Hours > 0)
            {
                format.Append($"{(this.timeSpan.TotalDays == 1 ? @"""24""" : @"h")}\\:");
            }

            if (this.Hours > 0 || this.Minutes > 0)
            {
                format.Append(
                    this.Hours > 0
                        ? @"mm\:"
                        : @"m\:");
            }

            format.Append(
                this.Hours > 0 || this.Minutes > 0
                    ? @"ss"
                    : @"s\s");

            return this.timeSpan.ToString(format.ToString());
        }

        public string ToLongString()
        {
            const int width = 2;
            const char padding = '0';

            return string.Format(
                "{0}:{1}:{2}",
                this.Hours.ToString().PadLeft(width, padding),
                this.Minutes.ToString().PadLeft(width, padding),
                this.Seconds.ToString().PadLeft(width, padding)
            );
        }

        public int CompareTo(Time other)
        {
            if (other == null) return 1;

            var comparison = this.Hours.CompareTo(other.Hours);
            if (comparison != 0) return comparison;

            comparison = this.Minutes.CompareTo(other.Minutes);
            if (comparison != 0) return comparison;

            comparison = this.Seconds.CompareTo(other.Seconds);
            if (comparison != 0) return comparison;

            return this.Milliseconds.CompareTo(other.Milliseconds);
        }

        public static implicit operator Time(TimeSpan timeSpan)
        {
            return new Time(timeSpan);
        }

        public static implicit operator TimeSpan(Time time)
        {
            if (null == time)
                throw new ArgumentNullException(nameof(time));

            return new TimeSpan(days: 0, time.Hours, time.Minutes, seconds: time.Seconds, time.Milliseconds);
        }

        public int Hours => this.timeSpan.TotalDays == 1 ? 24 : this.timeSpan.Hours;
        public int Minutes => this.timeSpan.Minutes;
        public int Seconds => this.timeSpan.Seconds;
        public int Milliseconds => this.timeSpan.Milliseconds;
    }
}
