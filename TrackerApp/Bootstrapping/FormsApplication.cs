﻿using System;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Web;
using TimeSuite.SuiteLib.Bootstrapping.IndividualComponents;
using TimeSuite.SuiteLib.Globals;
using TimeSuite.SuiteLib.Interfaces.Logging;
using TimeSuite.SuiteLib.Managers;
using TimeSuite.TrackerAsp;

namespace TimeSuite.TrackerApp.Bootstrapping
{
    public class FormsApplication : HttpApplication, IContainerProviderAccessor
    {
        public IContainerProvider ContainerProvider => GlobalVariables.CompositionRoot.ContainerProvider;

        public void Application_Start(object sender, EventArgs e)
        {
            LogUsageInformation($"{nameof(Application_Start)}({typeof(object).FullName}, {typeof(EventArgs).FullName})");
            OnAppStart();
        }

        /// <summary>
        /// This code executes on the application startup (Application_Start).
        /// </summary>
        private void OnAppStart()
        {
            LogUsageInformation($"{nameof(OnAppStart)}()");

            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            PurveyDataIntegrity();
        }

        private void PurveyDataIntegrity()
        {
            LogUsageInformation($"{nameof(PurveyDataIntegrity)}()");

            // TODO: Move this code to SuiteLib because only SuiteLib must know everyting about data integrity, not the app.
            using (var scope = GlobalVariables.CompositionRoot.LifetimeScope)
                scope.Resolve<DatabaseInitializer>().PurveyDataIntegrity();
        }

        private void LogUsageInformation(string methodSignature)
        {
            using (var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                var usageInformation = new TelemetryLogManager.UsageInformation($"{GetType().FullName}.{methodSignature}");
                scope.Resolve<IDisposableLogger<TelemetryLogManager.UsageInformation>>().Log(usageInformation);
            }
        }

        public void Application_Error(object sender, EventArgs e)
        {
            if (null != Server.GetLastError())
                Server.Transfer("~/Error.aspx");
        }
    }
}
