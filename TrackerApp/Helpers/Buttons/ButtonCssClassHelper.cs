﻿using System.Web.UI.WebControls;

namespace TimeSuite.TrackerApp.Helpers.Buttons
{
    public class ButtonCssClassHelper
    {
        public void Set(ButtonState buttonState, WebControl to)
        {
            to.Enabled = ButtonState.Enabled == buttonState;

            if (!to.Enabled)
                to.CssClass += " disabled";
        }
    }
}