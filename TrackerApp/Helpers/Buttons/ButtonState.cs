﻿namespace TimeSuite.TrackerApp.Helpers.Buttons
{
    public enum ButtonState
    {
        Enabled,
        Disabled
    }
}
