﻿using System.Data.Entity.Validation;
using System.Linq;

namespace TimeSuite.TrackerApp.Helpers.Data
{
    public static class DbEntityValidationHelper
    {
        public static string FormatMessage(DbEntityValidationException ex)
        {
            return string.Join(" ",
                ex.EntityValidationErrors.SelectMany(eve =>
                    eve.ValidationErrors.Select(ve => $"{ve.PropertyName}: {ve.ErrorMessage}")));
        }
    }
}
