﻿using System;
using System.Linq;
using System.Web.UI;
using TimeSuite.TrackerApp.Helpers.Reflection;

namespace TimeSuite.TrackerApp.Helpers.Pages
{
    public class PopupHelper
    {
        private readonly Type _templateAncestorType = typeof(TemplateControl);

        private readonly Type _templateControlType;
        private readonly ClientScriptManager _clientScriptManager;

        public PopupHelper(Type templateControlType, ClientScriptManager clientScriptManager)
        {
            if (null == templateControlType)
                throw new ArgumentNullException(nameof(templateControlType));
            else if (templateControlType.FindAncestors().Contains(_templateAncestorType))
                _templateControlType = templateControlType;
            else
                throw new ArgumentException($"Not a descendant of {_templateAncestorType}", nameof(templateControlType));

            if (null == clientScriptManager)
                throw new ArgumentNullException(nameof(clientScriptManager));
            else
                _clientScriptManager = clientScriptManager;
        }

        public void Popup(string message)
        {
            var messageHash = message.GetHashCode().ToString();

            if (_clientScriptManager.IsStartupScriptRegistered(_templateControlType, messageHash))
                return;

            _clientScriptManager.RegisterStartupScript(
                type: _templateControlType,
                key: messageHash,
                script: $"alert('{(message ?? string.Empty).Replace("'", @"\'")}')",
                addScriptTags: true);
        }
    }
}
