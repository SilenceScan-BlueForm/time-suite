﻿using System;
using System.Collections.Generic;

namespace TimeSuite.TrackerApp.Helpers.Reflection
{
    public static class TypeExtension
    {
        public static IEnumerable<Type> FindAncestors(this Type type)
        {
            if (null == type)
                yield break;

            var baseType = type.BaseType;
            while (null != baseType)
            {
                yield return baseType;
                baseType = baseType.BaseType;
            }
        }
    }
}
