﻿using System;
using System.Web.UI;
using Autofac;
using TimeSuite.SuiteLib.Globals;
using TimeSuite.SuiteLib.Interfaces.Logging;
using TimeSuite.SuiteLib.Managers;
using TimeSuite.TrackerApp.App_GlobalResources;
using TimeSuite.TrackerApp.Helpers.Pages;

namespace TimeSuite.TrackerApp.Pages
{
    public class FormPage : FormPageInternal
    {
        private PopupHelper _popupHelper;
        protected PopupHelper PopupHelper
        {
            get => _popupHelper ?? (_popupHelper = new PopupHelper(GetType(), ClientScript));
        }

        public FormPage()
        {
            LoadComplete += Page_LoadComplete;
        }

        protected sealed override void Page_Load(object sender, EventArgs e)
        {
            string pageName = GetType().BaseType.Name;
            string title = PagesTitles.ResourceManager.GetString(pageName);

            Page.Title = string.IsNullOrWhiteSpace(title) ? string.Format(PagesTitles._Unspecified, pageName) : title;

            OnPageLoad();
        }

        protected virtual void OnPageLoad()
        {
        }

        private void Page_LoadComplete(object sender, EventArgs e)
        {
            OnPageLoadComplete();
        }

        protected virtual void OnPageLoadComplete()
        {
        }

        protected void RedirectToDefaultDocument()
        {
            Response.Redirect("~/CurrentActivity.aspx");
        }

        protected override void RenderChildren(HtmlTextWriter writer)
        {
            base.RenderChildren(writer);

            LogMemoryUsage();
        }

        // TODO: Move the method to an HTTP module.
        private void LogMemoryUsage()
        {
            using (var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                scope.Resolve<IDisposableLogger<MemoryLogManager.WebFormName>>()
                    .Log(new MemoryLogManager.WebFormName(GetType().Name));
            }
        }

        private void Page_Unload(object sender, EventArgs e)
        {
            OnPageUnload();
        }

        protected virtual void OnPageUnload()
        {
        }
    }
}
