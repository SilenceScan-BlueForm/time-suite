﻿using System;
using System.Web.UI;

namespace TimeSuite.TrackerApp.Pages
{
    public abstract class FormPageInternal : Page
    {
        protected abstract void Page_Load(object sender, EventArgs e);
    }
}
