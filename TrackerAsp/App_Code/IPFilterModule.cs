﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using TimeSuite.TrackerAsp;

namespace BlueForm.App_Code
{
    public class IPFilterModule : IHttpModule
    {
        private const string AllowedIPAddressesSettingName = "AllowedIPAddresses";
        private const string[] Uninitialized = null;
        private readonly string[] Empty = new string[0];
        private const char IPAddressesSeparator = ',';
        private string[] _allowedIPAddresses = Uninitialized;
        private string[] AllowedIPAddresses
        {
            get
            {
                if (_allowedIPAddresses == Uninitialized)
                {
                    var allowedIPAddresses = ConfigurationManager.AppSettings[AllowedIPAddressesSettingName];
                    _allowedIPAddresses = string.IsNullOrWhiteSpace(allowedIPAddresses)
                        ? Empty
                        : allowedIPAddresses.Split(IPAddressesSeparator);
                }
                return _allowedIPAddresses;
            }
        }

        // TODO: Make the module to read the ForbiddenIPAddressPath optional application setting to show a custom Forbidden page.
        private const string Message = "Forbidden";

        public void Init(HttpApplication context)
        {
            context.BeginRequest += Application_BeginRequest;
        }

        public void Dispose()
        {
        }

        private void Application_BeginRequest(object sender, EventArgs e)
        {
            if (AllowedIPAddresses != Empty)
                ControlUserAccess(sender as Global);
        }

        private void ControlUserAccess(Global app)
        {
            if (!AllowedIPAddresses.Contains(app.Request.UserHostAddress))
                ForbidUserAccess(app.Context);
        }

        private void ForbidUserAccess(HttpContext context)
        {
            var encoding = context.Response.ContentEncoding;
            context.Response.OutputStream.Write(encoding.GetBytes(Message), 0, encoding.GetByteCount(Message));
            context.Response.ContentType = "text/html";
            context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
            context.Response.End();
        }
    }
}