﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Calendar.ascx.cs" Inherits="TimeSuite.TrackerAsp.Controls.Calendar" %>

<asp:Panel ID="CalendarPanel" runat="server" CssClass="calendar-border">
    <asp:Calendar ID="DateCalendar" runat="server" BorderStyle="None" CssClass="calendar" Width="100%">
        <DayHeaderStyle CssClass="day" />
        <DayStyle CssClass="day" />
        <NextPrevStyle Font-Bold="True" CssClass="day" />
        <OtherMonthDayStyle ForeColor="#777777" />
        <SelectedDayStyle BackColor="#337AB7" CssClass="active day" />
        <TitleStyle BackColor="transparent" Font-Bold="True" />
        <TodayDayStyle CssClass="today" />
    </asp:Calendar>
</asp:Panel>
