﻿using System;
using System.Web.UI.WebControls;

namespace TimeSuite.TrackerAsp.Controls
{
    public partial class Calendar : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DateCalendar.DayRender += DayRender;
            DateCalendar.SelectionChanged += SelectionChanged;
            DateCalendar.SelectionChanged += DateCalendar_SelectionChanged;
            MarkSelectedDateTodayOrNot();
        }

        public event DayRenderEventHandler DayRender;

        public event EventHandler SelectionChanged;

        private void DateCalendar_SelectionChanged(object sender, EventArgs e)
        {
            MarkSelectedDateTodayOrNot();
        }

        private void MarkSelectedDateTodayOrNot()
        {
            if (DateCalendar.SelectedDate == DateCalendar.TodaysDate)
                DateCalendar.SelectedDayStyle.CssClass += " today";
            else
                DateCalendar.SelectedDayStyle.CssClass = DateCalendar.SelectedDayStyle.CssClass.Replace(" today", string.Empty);
        }

        public DateTime SelectedDate
        {
            get => DateCalendar.SelectedDate;
            set => DateCalendar.SelectedDate = value;
        }

        public DateTime VisibleDate
        {
            get => DateCalendar.VisibleDate;
            set => DateCalendar.VisibleDate = value;
        }

        private const string mandatoryCssClass = "calendar-border ";
        public string CssClass
        {
            get => CalendarPanel.CssClass.Replace(mandatoryCssClass, string.Empty);
            set => CalendarPanel.CssClass = $"{mandatoryCssClass}{value}";
        }

        public Unit Width
        {
            get => CalendarPanel.Width;
            set => CalendarPanel.Width = value;
        }

        public dynamic ClientIDs
        {
            get
            {
                return new
                {
                    DateCalendar = DateCalendar.ClientID
                };
            }
        }
    }
}