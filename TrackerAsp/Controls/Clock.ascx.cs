﻿using System;
using System.Dynamic;
using System.Web.UI;
using TimeSuite.SuiteLib;

namespace TimeSuite.TrackerAsp.Controls
{
    public partial class Clock : UserControl
    {
        public Time SelectedTime
        {
            get
            {
                var hour = HoursDropDownList.SelectedValue;
                var minute = MinutesDropDownList.SelectedValue;
                var second = HiddenSeconds.Value;
                var millisecond = HiddenMilliseconds.Value;

                if (string.IsNullOrWhiteSpace(hour)
                    && string.IsNullOrWhiteSpace(minute))
                    return null;

                return new Time(
                    hours: string.IsNullOrWhiteSpace(hour) ? 0 : Convert.ToInt32(hour),
                    minutes: string.IsNullOrWhiteSpace(minute) ? 0 : Convert.ToInt32(minute),
                    seconds: /*TODO Handle exceptions*/int.Parse(second),
                    milliseconds: /*TODO Handle exceptions*/int.Parse(millisecond));
            }
            set
            {
                if (null == value)
                {
                    HoursDropDownList.SelectedValue =
                        MinutesDropDownList.SelectedValue =
                        string.Empty;
                }
                else
                {
                    HoursDropDownList.SelectedValue =
                        value.Hours.ToString().PadLeft(2, '0');

                    MinutesDropDownList.SelectedValue =
                        value.Minutes.ToString().PadLeft(2, '0');

                    HiddenSeconds.Value = (value?.Seconds).GetValueOrDefault(0).ToString();
                    HiddenMilliseconds.Value = (value?.Milliseconds).GetValueOrDefault(0).ToString();
                }
            }
        }

        public dynamic ClientIDs
        {
            get
            {
                dynamic ids = new ExpandoObject();
                ids.TimeMinute = MinutesDropDownList.ClientID;
                ids.TimeHour = HoursDropDownList.ClientID;
                return ids;
            }
        }

        protected void VisibleTimePartDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetHiddenTimeParts();
        }

        private void ResetHiddenTimeParts()
        {
            HiddenSeconds.Value = HiddenMilliseconds.Value = "0";
        }
    }
}