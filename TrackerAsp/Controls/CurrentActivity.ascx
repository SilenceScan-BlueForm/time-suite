﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CurrentActivity.ascx.cs" Inherits="TimeSuite.TrackerAsp.Controls.CurrentActivity" %>

<asp:Label ID="CaptionLabel" runat="server" Text="Current activity:" CssClass="text-muted"></asp:Label>
<asp:Label ID="Caption" runat="server" Text="Label" CssClass="text-info"></asp:Label>

<asp:Label ID="FreeAsABird" runat="server" Text="You're inactive" CssClass="text-warning" Visible="False"></asp:Label>
