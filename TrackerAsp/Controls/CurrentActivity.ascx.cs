﻿using System;
using Autofac;
using TimeSuite.SuiteLib.Globals;
using TimeSuite.SuiteLib.Interfaces.Repositories;
using TimeSuite.SuiteLib.Models;

namespace TimeSuite.TrackerAsp.Controls
{
    public partial class CurrentActivity : System.Web.UI.UserControl
    {
        public int? ActivityID { get; set; } = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadActivity();
        }

        private void LoadActivity()
        {
            using (var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                Activity currentActivity = scope.Resolve<IActivitiesRepository>().GetCurrent(userID: 1);

                string activityCaption = string.Empty;

                if (null != currentActivity)
                {
                    activityCaption = currentActivity.ActivityName;
                    ActivityID = currentActivity.ActivityID;
                }

                Set(activityCaption);
            }
        }

        public void Set(string caption)
        {
            if (!string.IsNullOrEmpty(caption))
            {
                Caption.Text = caption;
            }
            else
            {
                CaptionLabel.Visible = Caption.Visible = false;
                FreeAsABird.Visible = true;
            }
        }
    }
}