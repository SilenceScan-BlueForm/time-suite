﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditingActivity.ascx.cs" Inherits="TimeSuite.TrackerAsp.Controls.EditingActivity" %>
<%@ Register Src="~/Controls/Timestamp.ascx" TagPrefix="uc1" TagName="Timestamp" %>

<script runat="server">
    public string OKButtonText
    {
        get => OK.Text;
        set => OK.Text = value;
    }

    public string OnOKClientClick
    {
        get => OK.OnClientClick;
        set => OK.OnClientClick = value;
    }
</script>

<div class="row">
    <div class="col-xs-12">
        <asp:Panel ID="ActivityPanel" runat="server" CssClass="form-group">
            <asp:Label ID="ActivityNameLabel" runat="server"></asp:Label>

            <%-- TODO: Escaping of potentially dangerous characters
            Value caused the error is below:
            TODO Define T2 for Mapper<Contract, T2>: I assumed, it can be derived from General or extract all required fields from General.

            Error:
            A potentially dangerous Request.Form value was detected from the client (ctl00$MainContent$ActivityNameText="...для Mapper<Contract, T2>: скор..."). 
            Description: ASP.NET has detected data in the request that is potentially dangerous because it might include HTML markup or script. The data might represent
            an attempt to compromise the security of your application, such as a cross-site scripting attack. If this type of input is appropriate in your
            application, you can include code in a web page to explicitly allow it. For more information, see http://go.microsoft.com/fwlink/?LinkID=212874.

            Steps to be performed:
            1. handle error and show the user-friendly error instead of YSOD;
            2. implement a reusable web part for the text box allowing to escape potentially dangerous characters;
            --%>
            <asp:TextBox ID="ActivityNameText" runat="server" AutoPostBack="False" ViewStateMode="Inherit" CssClass="form-control">
            </asp:TextBox>
            <asp:RequiredFieldValidator ID="ActivityNameRequired" runat="server" ControlToValidate="ActivityNameText" CssClass="help-block">
            </asp:RequiredFieldValidator>
        </asp:Panel>
    </div>
</div>

<div class="row">
    <div class="col-xs-6">
        <asp:Panel runat="server" CssClass="panel panel-default" ID="StartTimestampPanel">
            <asp:Panel runat="server" CssClass="panel-heading" ID="StartTimestampHeadingPanel">
                <h4 class="panel-title"><asp:Literal runat="server" Text="Started"></asp:Literal></h4>
            </asp:Panel>
            <asp:Panel runat="server" CssClass="panel-body" ID="StartTimestampBodyPanel">
                <uc1:Timestamp runat="server" ID="StartTimestamp" />
            </asp:Panel>
        </asp:Panel>
    </div>
    <div class="col-xs-6">
        <asp:Panel runat="server" CssClass="panel panel-default" ID="StopTimestampPanel">
            <asp:Panel runat="server" CssClass="panel-heading" ID="StopTimestampHeadingPanel">
                <h4 class="panel-title"><asp:Literal runat="server" Text="Stopped"></asp:Literal></h4>
            </asp:Panel>
            <asp:Panel runat="server" CssClass="panel-body" ID="StopTimestampBodyPanel">
                <uc1:Timestamp runat="server" ID="StopTimestamp" />
            </asp:Panel>
        </asp:Panel>
    </div>
</div>

<asp:Panel ID="ButtonsPanel" runat="server" CssClass="form-control-static">
    <%-- TODO: Move this panel to a WebPart and reuse it in SwitchUnfinished.aspx --%>
    <asp:Button ID="OK" runat="server" OnClick="OK_Click" Text="OK" CssClass="btn btn-primary btn-lg" />
</asp:Panel>

<script type="text/javascript">
    let originalValidatorUpdateDisplay = window.ValidatorUpdateDisplay;
    window.ValidatorUpdateDisplay = function (obj) {
        if (typeof originalValidatorUpdateDisplay === "function") {
            originalValidatorUpdateDisplay(obj);
        }

        let control = obj ? document.getElementById(obj.controltovalidate) : null;
        let isValid = true;
        if (control && Array.isArray(control.Validators))
            for (let i in control.Validators)
                if (!control.Validators[i].isvalid) {
                    isValid = false;
                    break;
                }
        let ActivityPanel = document.getElementById("<%= this.FindControl("ActivityPanel").ClientID %>");
        if (isValid)
            ActivityPanel.classList.remove("has-error");
        else
            ActivityPanel.classList.add("has-error");
    }
</script>

<script type="text/javascript">
    let editingActivityDisable = "EditingActivityDisable";
    if (typeof window[editingActivityDisable] !== "function") {
        // TODO Move the definition below into bundle to enable caching of this function.
        window[editingActivityDisable] = function (editingActivityControlClientID) {
            let clientIDs = window[editingActivityDisable][instancesClientIDs][editingActivityControlClientID];

            let okInput = document.getElementById(clientIDs.OKSubmitInput);
            if (Util.Input.Disabled(okInput)) {
                return false;
            } else {
                Util.Disable.SubmitButtonInput(okInput);
                Util.Disable.TextBoxInput(document.getElementById(clientIDs.ActivityNameText));

                Util.Disable.AspNetCalendar(document.getElementById(clientIDs.StartTimestampDateCalendar));
                Util.Disable.DropDownList(document.getElementById(clientIDs.StartTimestampTimeHour));
                Util.Disable.DropDownList(document.getElementById(clientIDs.StartTimestampTimeMinute));

                Util.Disable.AspNetCalendar(document.getElementById(clientIDs.StopTimestampDateCalendar));
                Util.Disable.DropDownList(document.getElementById(clientIDs.StopTimestampTimeHour));
                Util.Disable.DropDownList(document.getElementById(clientIDs.StopTimestampTimeMinute));
                return true;
            }
        }
    }

    let instancesClientIDs = "InstancesClientIDs";

    if (!window[editingActivityDisable][instancesClientIDs]) {
        window[editingActivityDisable][instancesClientIDs] = {};
    }

    /* This magical IDs passing is here because of the possibility to put several EditingActivity control instances on the web form.
     * The EditingActivity control must give the function to lock its instances separately.
     * The code below makes it possible to lock EditingActivity controls instances by passing its client IDs.
     * I didn't inject client IDs right into the EditingActivityDisable() because I don't want ASP.NET to repeat that function multiple times. */
    window[editingActivityDisable][instancesClientIDs]["<%= ClientID %>"] = {
        OKSubmitInput: "<%= ClientIDs.OKSubmitInput %>",
        ActivityNameText: "<%= ClientIDs.ActivityNameText %>",
        StartTimestampDateCalendar: "<%= ClientIDs.StartTimestampDateCalendar %>",
        StartTimestampTimeHour: "<%= ClientIDs.StartTimestampTimeHour %>",
        StartTimestampTimeMinute: "<%= ClientIDs.StartTimestampTimeMinute %>",
        StopTimestampDateCalendar: "<%= ClientIDs.StopTimestampDateCalendar %>",
        StopTimestampTimeHour: "<%= ClientIDs.StopTimestampTimeHour %>",
        StopTimestampTimeMinute: "<%= ClientIDs.StopTimestampTimeMinute %>"
    };
</script>
