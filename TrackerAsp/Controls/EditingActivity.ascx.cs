﻿using System;
using System.Dynamic;
using TimeSuite.SuiteLib;
using TimeSuite.TrackerApp.App_GlobalResources;

namespace TimeSuite.TrackerAsp.Controls
{
    public partial class EditingActivity : System.Web.UI.UserControl
    {
        public string ActivityName
        {
            get => ActivityNameText.Text;
            set => ActivityNameText.Text = value;
        }

        public DateTime? SelectedStartTimestamp
        {
            get
            {
                return BuildNullableTimestamp(StartTimestamp.SelectedDate, StartTimestamp.SelectedTime);
            }
            set
            {
                StartTimestamp.SelectedDate = value?.Date;
                StartTimestamp.SelectedTime = value?.TimeOfDay;
            }
        }

        public DateTime? SelectedStopTimestamp
        {
            get
            {
                return BuildNullableTimestamp(StopTimestamp.SelectedDate, StopTimestamp.SelectedTime);
            }
            set
            {
                StopTimestamp.SelectedDate = value?.Date;
                StopTimestamp.SelectedTime = value?.TimeOfDay;
            }
        }

        private DateTime? BuildNullableTimestamp(DateTime? date, Time time)
        {
            var hasDate = date.HasValue;
            string message;

            if (hasDate &&
                null != time)
                return date.Value.Add(time);
            else if (!hasDate &&
                null == time)
                return null;
            else if (!hasDate)
                message = "Fill in a date or clear the time";
            else
                message = "Fill in a time"; //TODO for now it's impossible to reset a date

            throw new InvalidOperationException(message);
        }

        // TODO: Move to .ascx (just like OKButtonText) to be able to set in .aspx files.
        public event EventHandler ActivityEditAccepted;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ActivityNameLabel.Text = LabelTexts.ActivityName;
                ActivityNameRequired.ErrorMessage = $"Please enter {LabelTexts.ActivityName}";
            }
        }

        protected void OK_Click(object sender, EventArgs e)
        {
            EventHandler handler = ActivityEditAccepted;
            handler?.Invoke(sender, e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (!ActivityNameRequired.IsValid)
                ActivityPanel.CssClass += " has-error";
        }

        public dynamic ClientIDs
        {
            get
            {
                dynamic ids = new ExpandoObject();

                ids.ActivityNameText = ActivityNameText.ClientID;

                ids.StartTimestampDateCalendar = StartTimestamp.ClientIDs.DateCalendar;
                ids.StartTimestampTimeHour = StartTimestamp.ClientIDs.TimeHour;
                ids.StartTimestampTimeMinute = StartTimestamp.ClientIDs.TimeMinute;

                ids.StopTimestampDateCalendar = StopTimestamp.ClientIDs.DateCalendar;
                ids.StopTimestampTimeHour = StopTimestamp.ClientIDs.TimeHour;
                ids.StopTimestampTimeMinute = StopTimestamp.ClientIDs.TimeMinute;

                ids.OKSubmitInput = OK.ClientID;

                return ids;
            }
        }
    }
}