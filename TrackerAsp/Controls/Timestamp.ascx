﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Timestamp.ascx.cs" Inherits="TimeSuite.TrackerAsp.Controls.Timestamp" %>
<%@ Register Src="~/Controls/Calendar.ascx" TagPrefix="bs3" TagName="Calendar" %>
<%@ Register Src="~/Controls/Clock.ascx" TagPrefix="uc" TagName="Clock" %>


<asp:Panel ID="TimestampPanel" runat="server" CssClass="form-group row">
    <asp:Panel ID="DatePanel" runat="server" CssClass="col-xs-8">
        <asp:HiddenField ID="IsDateNull" runat="server" />
        <h6 class="text-center"><asp:Literal runat="server" Text="Date"></asp:Literal></h6>
        <bs3:Calendar runat="server" ID="DateCalendar"
            OnDayRender="DateCalendar_DayRender" OnSelectionChanged="DateCalendar_SelectionChanged">
        </bs3:Calendar>
    </asp:Panel>
    <asp:Panel ID="TimePanel" runat="server" CssClass="col-xs-4">
        <h6 class="text-center"><asp:Literal runat="server" Text="Time"></asp:Literal></h6>
        <uc:Clock runat="server" ID="TimeClock" />
    </asp:Panel>
    <%-- TODO: Validate (using RequiredFieldValidator) that StartTimestamp is not null --%>
</asp:Panel>
