﻿using System;
using System.Dynamic;
using System.Web.UI;
using System.Web.UI.WebControls;
using TimeSuite.SuiteLib;

namespace TimeSuite.TrackerAsp.Controls
{
    public partial class Timestamp : UserControl
    {
        public DateTime? SelectedDate
        {
            get
            {
                return bool.Parse(IsDateNull.Value)
                    ? (DateTime?) null
                    : DateCalendar.SelectedDate;
            }
            set
            {
                var isDateNull = SetAndGetIsDateNull(value);

                if (!isDateNull)
                    DateCalendar.SelectedDate = value.Value.Date;
            }
        }

        public Time SelectedTime
        {
            get => TimeClock.SelectedTime;
            set => TimeClock.SelectedTime = value;
        }

        protected void DateCalendar_DayRender(object sender, DayRenderEventArgs e)
        {
            if (e.Day.Date > DateTime.Today)
            {
                e.Day.IsSelectable = false;
            }
        }

        protected void DateCalendar_SelectionChanged(object sender, EventArgs e)
        {
            _ = SetAndGetIsDateNull(DateCalendar.SelectedDate);
        }

        private bool SetAndGetIsDateNull(DateTime? value)
        {
            var isDateNull = !value.HasValue;
            IsDateNull.Value = isDateNull.ToString();

            return isDateNull;
        }

        public dynamic ClientIDs
        {
            get
            {
                dynamic ids = new ExpandoObject();
                ids.DateCalendar = DateCalendar.ClientIDs.DateCalendar;
                ids.TimeHour = TimeClock.ClientIDs.TimeHour;
                ids.TimeMinute = TimeClock.ClientIDs.TimeMinute;
                return ids;
            }
        }
    }
}