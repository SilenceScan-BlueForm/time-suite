﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" Inherits="CurrentActivity" Codebehind="CurrentActivity.aspx.cs" %>
<%@ Import Namespace="TimeSuite.TrackerApp.App_GlobalResources" %>

<%@ Register src="Controls/CurrentActivity.ascx" tagname="CurrentActivity" tagprefix="uc1" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>
            <asp:Label ID=WorkedOutTimeLabel runat="server" Text="Time Tracker"></asp:Label>
        </h1>
        <p class="lead">
            <uc1:CurrentActivity ID="CurrentActivityCtrl" runat="server" />
        </p>
        <div>
            <asp:HyperLink ID="EditLink" runat="server" NavigateUrl="~/EditActivity.aspx" CssClass="btn btn-info btn-lg"><%: PagesTitles.EditActivity %> &raquo;</asp:HyperLink>

            <div class="btn-group-vertical">
                <asp:LinkButton runat="server" ID="SwitchUnfinishedLink" PostBackUrl="~/SwitchUnfinished.aspx" CssClass="btn btn-primary btn-lg"><%: PagesTitles.SwitchUnfinished %> &raquo;</asp:LinkButton>
                <asp:LinkButton runat="server" ID="SwitchNewLink" PostBackUrl="~/SwitchNew.aspx" class="btn btn-primary btn-lg"><%: PagesTitles.SwitchNew %> &raquo;</asp:LinkButton>
            </div>

            <a runat="server" href="~/StopCurrentActivity.aspx" class="btn btn-danger btn-lg" id="StopAnchor"><%: PagesTitles.Stop %> &raquo;</a>

            <a runat="server" href="~/Report.aspx" class="btn btn-success btn-lg"><%: PagesTitles.Report %> &raquo;</a>
        </div>
    </div>
</asp:Content>
