﻿using System;
using Autofac;
using TimeSuite.SuiteLib.Globals;
using TimeSuite.SuiteLib.Interfaces.Repositories;
using TimeSuite.TrackerApp.Helpers.Buttons;
using TimeSuite.TrackerApp.Pages;

public partial class CurrentActivity : FormPage
{
    readonly ButtonCssClassHelper btnCssClsHelper = new ButtonCssClassHelper();

    protected override void OnPageLoad()
    {
        using(var scope = GlobalVariables.CompositionRoot.LifetimeScope)
        {
            var activitiesRepo = scope.Resolve<IActivitiesRepository>();

            // TODO: Pass `userID`s to all methods accepting such parameter in all the solution from somewhere.
            WorkedOutTimeLabel.Text = activitiesRepo
                .DayWorkedOutDuration(DateTime.Now, userID: 1).ToLongString();
            
            btnCssClsHelper.Set(GetSwitchUnfinishedState(activitiesRepo),
                to: SwitchUnfinishedLink);
        }
    }

    private ButtonState GetSwitchUnfinishedState(IActivitiesRepository repo)
    {
        return repo
            .GetUnfinishedExceptCurrentActivitiesCount(userID: 1) != 0
            ? ButtonState.Enabled
            : ButtonState.Disabled;
    }

    protected override void OnPageLoadComplete()
    {
        SetupStopButton();
        SetupEditButton();
    }

    private void SetupStopButton()
    {
        StopAnchor.Visible = CurrentActivityCtrl.ActivityID.HasValue;
    }

    private void SetupEditButton()
    {
        btnCssClsHelper.Set(GetEditCurrentState(),
            to: EditLink);

        if (EditLink.Enabled)
            EditLink.NavigateUrl =
                $"{EditLink.NavigateUrl}?id={CurrentActivityCtrl.ActivityID.Value}";
    }

    private ButtonState GetEditCurrentState()
    {
        return CurrentActivityCtrl.ActivityID.HasValue
            ? ButtonState.Enabled
            : ButtonState.Disabled;
    }
}
