﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditActivity.aspx.cs" Inherits="TimeSuite.TrackerAsp.EditActivity" MasterPageFile="~/Site.Master" %>
<%@ Import Namespace="TimeSuite.TrackerApp.App_GlobalResources" %>
<%@ Register Src="~/Controls/EditingActivity.ascx" TagPrefix="uc1" TagName="EditingActivity" %>


<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function disableActivity() {
            return window.EditingActivityDisable("<%= EditingActivity.ClientID %>");
        }
    </script>

    <uc1:EditingActivity runat="server" ID="EditingActivity" OKButtonText="Save changes" OnOKClientClick="return disableActivity()" />
</asp:Content>
