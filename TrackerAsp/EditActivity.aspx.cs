﻿using System;
using System.Data.Entity.Validation;
using System.Text;
using Autofac;
using TimeSuite.SuiteLib.Globals;
using TimeSuite.SuiteLib.Interfaces.Repositories;
using TimeSuite.SuiteLib.Models;
using TimeSuite.TrackerApp.App_GlobalResources;
using TimeSuite.TrackerApp.Helpers.Data;
using TimeSuite.TrackerApp.Pages;

namespace TimeSuite.TrackerAsp
{
    public partial class EditActivity : FormPage
    {
        private const string _notFoundOrForbidden = "The requested activity does not exist or does not belong to you";
        private const string _noCurrentActivity = "There is no an activity to edit because you are inactive at the moment";

        protected override void OnPageLoad()
        {
            if (!IsPostBack)
                InitForm();

            EditingActivity.ActivityEditAccepted += ActivityEdit_Accepted;
        }

        private void InitForm()
        {
            var editingActivityData = GetEditingActivity();
            EditingActivity.ActivityName = editingActivityData.ActivityName; // TODO: Replace with controls data binding (project wide).

            var lastTiming = GetLastTiming(editingActivityData.ActivityID);

            if (null != lastTiming)
            {
                EditingActivity.SelectedStartTimestamp = lastTiming.StartTimestamp;
                EditingActivity.SelectedStopTimestamp = lastTiming.StopTimestamp;
            }
        }

        private Activity GetEditingActivity()
        {
            var userID = 1; // TODO: Pass `userID`s to all methods accepting such parameter in all the solution from somewhere.
            string noActivityReason = null;
            Activity activity = null;

            using (var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                var repo = scope.Resolve<IActivitiesRepository>();

                if (TryGetActivityIDToEditFromQueryString(out var activityID))
                {
                    activity = repo.GetByID(activityID, userID);
                    noActivityReason = null == activity ? _notFoundOrForbidden : string.Empty;
                }
                else
                {
                    activity = repo.GetCurrent(userID);
                    noActivityReason = null == activity ? _noCurrentActivity : string.Empty;
                }
            }

            if (null == activity)
                throw new InvalidOperationException(noActivityReason);
            else
                return activity;
        }

        private ActivityTiming GetLastTiming(int activityID)
        {
            using (var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                var repo = scope.Resolve<IActivitiesRepository>();

                return repo.GetLastActivityTiming(activityID);
            }
        }

        private bool TryGetActivityIDToEditFromQueryString(out int activityID)
        {
            return int.TryParse(Request.QueryString["id"], out activityID);
        }

        private void ActivityEdit_Accepted(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(EditingActivity.ActivityName))
            {
                PopupHelper.Popup(Messages.ActivityNameEmpty);
                return;
            }

            // TODO: Use such DB errors handling for all the handlers of all the forms.

            var editingActivity = GetEditingActivity();

            // TODO: Use transactions for consistent updates (don't update timing if activity updating has failed).
            PutActivity(editingActivity, out var fail);
            if (!string.IsNullOrEmpty(fail))
            {
                PopupHelper.Popup(fail);
                return;
            }

            PutActivityTiming(editingActivity, out fail);

            if (string.IsNullOrEmpty(fail))
                RedirectToDefaultDocument();
            else
                PopupHelper.Popup(fail);
        }

        private void PutActivity(Activity editingActivity, out string putFailReason)
        {
            putFailReason = string.Empty;

            // TODO: Learn pages lifecycle and prevent double call to the DB to get activity ID (preserve it).

            // TODO: Fix a case: a user has a current activity and 
            // opened this page without 'id' query string parameter,
            // then stopped the current activity in another tab. Then
            // she/he returned back to this page and pressed Save Changes.
            // The value of the activityID will be null because there is
            // no 'id' parameter in the query string and there is no
            // current activity. Probably, the simplest solution is to
            // prevent opening the form without the 'id' query string param.

            editingActivity.ActivityName = EditingActivity.ActivityName;

            using (var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                try
                {
                    // TODO: Checkup one more time: is this really singleton?
                    scope.Resolve<IActivitiesRepository>().PutActivity(editingActivity.ActivityID,
                        editingActivity);
                }
                catch (DbEntityValidationException ex)
                {
                    // TODO: Catch and popup alerts with entities validation exceptions automatically accross the project.
                    putFailReason = DbEntityValidationHelper.FormatMessage(ex);
                }
            }
        }

        private void PutActivityTiming(Activity editingActivity, out string putFailReason)
        {
            ValidateEditingActivity(out putFailReason);
            if (!string.IsNullOrEmpty(putFailReason))
                return;

            // TODO: Try to use model and bind forms to it.
            var editingTiming = GetLastTiming(editingActivity.ActivityID)
                ?? new ActivityTiming { ActivityID = editingActivity.ActivityID };

            UpdateActivityTiming(editingTiming);

            using (var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                try
                {
                    // Try validate without exceptions throwing (because it's slow).
                    scope.Resolve<IActivitiesRepository>().PutActivityTiming(editingTiming.TimingID,
                        editingTiming);
                }
                catch (DbEntityValidationException ex)
                {
                    putFailReason = DbEntityValidationHelper.FormatMessage(ex);
                }
            }
        }

        private void ValidateEditingActivity(out string fail)
        {
            const string startTimestampCaption = "Start";
            const string stopTimestampCaption = "Stop";

            const string futureTimestampFormat = "{0}{1} timestamp of an activity must not be in a future.";
            const string wrongTimestampFormat = "{0}{1} timestamp: {2}";

            const string separator = @"\n";
            string separate(StringBuilder builder) => builder.Length > 0 ? separator : string.Empty;
            var message = new StringBuilder();

            DateTime? startTimestamp = null;
            bool exceptionCaught = false;
            try
            {
                startTimestamp = EditingActivity.SelectedStartTimestamp.Value;
            }
            catch (InvalidOperationException ex)
            {
                message.AppendFormat(wrongTimestampFormat, separate(message), startTimestampCaption, ex.Message);
                exceptionCaught = true;
            }

            if (!exceptionCaught && !startTimestamp.HasValue)
            {
                message.Append("Timings of all activities must have a start timestamp.");
            }

            if (startTimestamp.HasValue && startTimestamp.Value > DateTime.Now)
            {
                message.AppendFormat(futureTimestampFormat, separate(message), startTimestampCaption);
            }

            DateTime? stopTimestamp = null;
            try
            {
                stopTimestamp = EditingActivity.SelectedStopTimestamp;
            }
            catch (InvalidOperationException ex)
            {
                message.AppendFormat(wrongTimestampFormat, separate(message), stopTimestampCaption, ex.Message);
            }

            if (stopTimestamp.HasValue &&
                stopTimestamp.Value > DateTime.Now)
            {
                message.AppendFormat(futureTimestampFormat, separate(message), stopTimestampCaption);
            }

            fail = message.ToString();
        }

        private void UpdateActivityTiming(ActivityTiming editingTiming)
        {
            editingTiming.StartTimestamp = EditingActivity.SelectedStartTimestamp.Value;
            editingTiming.StopTimestamp = EditingActivity.SelectedStopTimestamp;
        }
    }
}