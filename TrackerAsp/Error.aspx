﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="TimeSuite.TrackerAsp.Error" %>
<%@ Import Namespace="TimeSuite.TrackerApp.App_GlobalResources" %>

<asp:Content ContentPlaceHolderID="MainContent"  ID="ErrorContent" runat="server">
    <pre class="text-danger"><%: Server.GetLastError().GetBaseException().Message ?? "An exception is occured" %></pre>
</asp:Content>
