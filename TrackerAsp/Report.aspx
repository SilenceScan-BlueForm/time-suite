﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Report.aspx.cs" Inherits="TimeSuite.TrackerAsp.Report" %>
<%@ Import Namespace="TimeSuite.TrackerApp.App_GlobalResources" %>
<%@ Register Src="~/Controls/Calendar.ascx" TagPrefix="bs3" TagName="Calendar" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h3><%: ReportDate.SelectedDate.ToLongDateString() %></h3>
    <h4>
        <asp:Label ID=WorkedOutTimeLabel runat="server" Text="Time Tracker"></asp:Label>
    </h4>
    <div class="row">
        <div class="col-xs-9">
            <asp:GridView ID="ActivitiesReport" runat="server" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField DataField="ActivityName" HeaderText="Activity Name" ReadOnly="True" />
                    <asp:BoundField DataField="ElapsedTime" DataFormatString="{0:hh\:mm\:ss}"
                        HeaderText="Elapsed time ([[hours:]minutes:]seconds)" ReadOnly="True" >
                    <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
        </div>
        <div class="col-xs-3">
            <bs3:Calendar ID="ReportDate" runat="server" SelectedDate="<%# DateTime.Today %>" VisibleDate="<%# DateTime.Today %>"
                OnSelectionChanged="ReportDate_SelectionChanged">
            </bs3:Calendar>
        </div>
    </div>
    <p>
        <asp:Button ID="OK" runat="server" Text="<%# ButtonTexts.Back %>" CssClass="btn btn-success btn-lg"
            OnClick="OK_Click" />
    </p>
</asp:Content>
