﻿using System;
using Autofac;
using TimeSuite.SuiteLib.Globals;
using TimeSuite.SuiteLib.Interfaces.Repositories;
using TimeSuite.TrackerApp.Pages;

namespace TimeSuite.TrackerAsp
{
    public partial class Report : FormPage
    {
        protected override void OnPageLoad()
        {
            if(IsPostBack)
            {
                return;
            }

            Page.DataBind();
            RefreshData();
        }

        private void RefreshData()
        {
            using(var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                var activitiesRepo = scope.Resolve<IActivitiesRepository>();
                var reportDate = ReportDate.SelectedDate;
                var userID = 1;

                ActivitiesReport.DataSource = activitiesRepo.GetByDate(reportDate, userID);
                ActivitiesReport.DataBind();

                WorkedOutTimeLabel.Text = activitiesRepo.DayWorkedOutDuration(reportDate, userID).ToLongString();
            }
        }

        protected void OK_Click(object sender, EventArgs e)
        {
            RedirectToDefaultDocument();
        }

        protected void ReportDate_SelectionChanged(object sender, EventArgs e)
        {
            RefreshData();
        }
    }
}