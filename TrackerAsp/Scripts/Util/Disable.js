﻿if (!window.Util) window.Util = {};
if (!window.Util.Disable) window.Util.Disable = {

  SubmitButtonInput: function (htmlSubmitInputElement) {
    if (!htmlSubmitInputElement)
      return;

    htmlSubmitInputElement.classList.add("disabled");
  }
  ,
  TextBoxInput: function (htmlTextInputElement) {
    if (!htmlTextInputElement)
      return;

    htmlTextInputElement.setAttribute("readonly", "readonly");
  }
  ,
  AspNetCalendar: function (aspCalendarHTMLElement) {
    if (!aspCalendarHTMLElement)
      return;

    function removeLinksFromTable(htmlTableElement) {
      for (let r in htmlTableElement.rows) {
        let row = htmlTableElement.rows[r];
        for (let c in row.cells) {
          let cell = row.cells[c];
          if (cell.firstElementChild == null)
            continue;
          else if (cell.firstElementChild.tagName == "TABLE")
            removeLinksFromTable(cell.firstElementChild);
          else if (cell.firstElementChild.tagName == "A")
            cell.textContent = cell.firstElementChild.textContent;
        }
      }
    }

    function deactivateTable(htmlTableElement) {
      if (htmlTableElement && htmlTableElement.tagName === "TABLE")
        htmlTableElement.classList.add("table-inactive");
    }

    removeLinksFromTable(aspCalendarHTMLElement);
    deactivateTable(aspCalendarHTMLElement);

    let monthNavigator = aspCalendarHTMLElement.rows[0].cells[0].firstElementChild;
    deactivateTable(monthNavigator);
  }
  ,
  DropDownList: function (htmlSelectElement) {
    if (!htmlSelectElement)
      return;

    htmlSelectElement.setAttribute("readonly", "readonly");
    for (let i = 0; i < htmlSelectElement.options.length; ++i) {
      let option = htmlSelectElement.options[i];
      if (!option.selected) {
        option.setAttribute("disabled", "disabled");
      }
    }
  }
  ,
  RadioButtonList: function (radioButtonsTableElement) {
    if (!radioButtonsTableElement)
      return;

    for (let r = 0; r < radioButtonsTableElement.rows.length; ++r) {
      let row = radioButtonsTableElement.rows[r];
      for (let c = 0; c < row.cells.length; ++c) {
        let cell = row.cells[c];
        let radioInput = cell.firstElementChild;
        if (!radioInput.checked) radioInput.disabled = true;
      }
    }
  }

};
