﻿if (!window.Util) window.Util = {};
if (!window.Util.Input) window.Util.Input = {

  Disabled: function (input) {
    return !!input
      && (!input.classList
      || input.classList.__proto__.constructor !== DOMTokenList
      || typeof input.classList.contains !== "function"
      || input.classList.contains("disabled"));
  }

};
