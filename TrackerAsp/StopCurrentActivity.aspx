﻿<%@ Page
    Title="Stop the current activity"
    Language="C#"
    MasterPageFile="~/Site.Master"
    AutoEventWireup="true"
    Inherits="TimeSuite.TrackerAsp.Stop"
    Codebehind="StopCurrentActivity.aspx.cs"
%>

<%@ Import Namespace="TimeSuite.TrackerApp.App_GlobalResources" %>

<%@ Register Src="~/Controls/CurrentActivity.ascx" TagPrefix="uc1" TagName="CurrentActivity" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function disableForm() {
            let pauseInput = document.getElementById("<%= Pause.ClientID %>");
            if (Util.Input.Disabled(pauseInput)) {
                return false; // prevent execution of further (server side) event handling
            } else {
                Util.Disable.SubmitButtonInput(pauseInput);
                Util.Disable.SubmitButtonInput(document.getElementById("<%= Back.ClientID %>"));
                Util.Disable.SubmitButtonInput(document.getElementById("<%= Finish.ClientID %>"));
                return true;
            }
        }
    </script>

    <h3>The next activity will be stopped</h3>
    <p>
        <uc1:CurrentActivity runat="server" ID="CurrentActivityCtrl" />
    </p>
    <p>
        <asp:Button ID="Back" runat="server" Text="<%# ButtonTexts.Back %>" CssClass="btn btn-success btn-lg" ToolTip="Return back to the main form."
            OnClick="Back_Click" OnClientClick="return disableForm()" />
        
        <asp:Button ID="Pause" runat="server" Text="Pause the activity" CssClass="btn btn-primary btn-lg" ToolTip="Pause the current activity to return to it later."
            OnClick="Pause_Click" OnClientClick="return disableForm()" />
        
        <asp:Button ID="Finish" runat="server" Text="Finish the activity" CssClass="btn btn-danger btn-lg" ToolTip="Mark the current activity as finished."
            OnClick="Finish_Click" OnClientClick="return disableForm()"/>
    </p>
</asp:Content>
