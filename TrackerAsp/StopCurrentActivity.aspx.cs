﻿using System;
using Autofac;
using TimeSuite.SuiteLib.Globals;
using TimeSuite.SuiteLib.Interfaces.Repositories;
using TimeSuite.TrackerApp.Pages;

namespace TimeSuite.TrackerAsp
{
    public partial class Stop : FormPage
    {
        protected void Back_Click(object sender, EventArgs e)
        {
            RedirectToDefaultDocument();
        }

        protected void Pause_Click(object sender, EventArgs e)
        {
            if(CurrentActivityCtrl.ActivityID.HasValue)
                StopActivity(CurrentActivityCtrl.ActivityID.Value);

            RedirectToDefaultDocument();
        }

        protected void Finish_Click(object sender, EventArgs e)
        {
            if(CurrentActivityCtrl.ActivityID.HasValue)
            {
                int activityID = CurrentActivityCtrl.ActivityID.Value;
                StopActivity(activityID);
                FinishActivity(activityID);
            }

            RedirectToDefaultDocument();
        }

        private void StopActivity(int activityID)
        {
            using(var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                scope.Resolve<IActivitiesRepository>().StopOne(activityID);
            }
        }

        private void FinishActivity(int activityID)
        {
            using(var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                scope.Resolve<IActivitiesRepository>().FinishOne(activityID);
            }
        }

        protected override void OnPageLoad()
        {
            if(IsPostBack)
            {
                return;
            }

            Page.DataBind();
        }

        protected override void OnPageLoadComplete()
        {
            if(!CurrentActivityCtrl.ActivityID.HasValue)
                Pause.Visible = Finish.Visible = false;
        }
    }
}