﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" Inherits="TimeSuite.TrackerAsp.SwitchNew" Codebehind="SwitchNew.aspx.cs" %>
<%@ Import Namespace="TimeSuite.TrackerApp.App_GlobalResources" %>
<%@ Register Src="~/Controls/EditingActivity.ascx" TagPrefix="uc1" TagName="EditingActivity" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function disableActivity() {
            return window.EditingActivityDisable("<%= EditingActivity.ClientID %>");
        }
    </script>

    <uc1:EditingActivity runat="server" id="EditingActivity" OKButtonText="Switch to it" OnOKClientClick="return disableActivity()" />
</asp:Content>
