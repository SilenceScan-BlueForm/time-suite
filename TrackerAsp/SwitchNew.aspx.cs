﻿using System;
using System.Data.Entity.Validation;
using Autofac;
using TimeSuite.SuiteLib.Globals;
using TimeSuite.SuiteLib.Interfaces.Repositories;
using TimeSuite.TrackerApp.App_GlobalResources;
using TimeSuite.TrackerApp.Helpers.Data;
using TimeSuite.TrackerApp.Pages;

namespace TimeSuite.TrackerAsp
{
    public partial class SwitchNew : FormPage
    {
        protected override void OnPageLoad()
        {
            // TODO: Set this in the .aspx file (fix control for this firstly).
            EditingActivity.ActivityEditAccepted += ActivityAndTiming_Accepted;
        }

        protected void ActivityAndTiming_Accepted(object sender, EventArgs e)
        {
            SwitchActivity();
        }

        private void SwitchActivity()
        {
            if (string.IsNullOrEmpty(EditingActivity.ActivityName))
            {
                PopupHelper.Popup(Messages.ActivityNameEmpty);
                return;
            }

            try
            {
                // TODO Make the operations transactional (to not stopping the current activity if a new activity is wrong).
                StopCurrentActivity();
                SwitchToNewActivity();

                RedirectToDefaultDocument();
            }
            catch (DbEntityValidationException ex)
            {
                // TODO: Catch and popup alerts with entities validation exceptions automatically accross the project.
                PopupHelper.Popup(DbEntityValidationHelper.FormatMessage(ex));
            }
        }
        
        private void StopCurrentActivity()
        {
            // TODO: Move this method from the current class and also from the SwitchUnfinished.aspx.cs too to the `SwitchActivityFormPage:FormPage`.
            // TODO: Make the repository singleton using a DI container (in all the code files).
            using(var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                var repo = scope.Resolve<IActivitiesRepository>();
                int? currentActivityID = repo.GetCurrent(userID: 1)?.ActivityID;

                if (currentActivityID.HasValue)
                {
                    repo.StopOne(currentActivityID.Value);
                }
            }
        
        }

        private void SwitchToNewActivity()
        {
            using(var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                // TODO: Replace userID:1 with getting the current user ID from any architecturally correct place.
                scope.Resolve<IActivitiesRepository>().Create(EditingActivity.ActivityName, userID: 1);
            }
        }

        protected override void OnPageUnload()
        {
            EditingActivity.ActivityEditAccepted -= ActivityAndTiming_Accepted;
        }
    }
}