﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" Inherits="TimeSuite.TrackerAsp.SwitchUnfinished" Codebehind="SwitchUnfinished.aspx.cs" %>
<%@ Import Namespace="TimeSuite.TrackerApp.App_GlobalResources" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function disableActivities() {
            let okInput = document.getElementById("<%= OK.ClientID %>");
            let createInput = document.getElementById("<%= SwitchNew.ClientID %>");
            if (Util.Input.Disabled(okInput)) {
                return false; // prevent execution of further (server side) event handling
            } else {
                Util.Disable.SubmitButtonInput(okInput);
                Util.Disable.SubmitButtonInput(createInput);
                Util.Disable.RadioButtonList(document.getElementById("<%= ActivitiesList.ClientID %>"));
                return true; // continue execution of further (server side) event handling
            }
        }
    </script>

    <!-- TODO Move out from this page the NoUnfinishedActivities and the ThereAreUnfinishedActivities panels to separate controls and use them in MultiView here -->

    <asp:Panel ID="NoUnfinishedActivities" runat="server" Visible="false">
        <asp:Literal ID=NoUnfinishedActivitiesLiteral runat="server" Text="<%# Texts.NoUnfinishedActivities %>"></asp:Literal>
    </asp:Panel>

    <asp:Panel ID="ThereAreUnfinishedActivities" runat="server" CssClass="form-group has-error" Visible="false">
        <asp:Label ID="UserActionRequestLabel" runat="server" Text="Select an unfinished activity you would like to switch to"></asp:Label>
        <asp:RadioButtonList ID="ActivitiesList" runat="server" DataTextField="ActivityName" DataValueField="ActivityID">
        </asp:RadioButtonList>
        <asp:RequiredFieldValidator ID="ActivityRequired" runat="server"
            ControlToValidate="ActivitiesList" ErrorMessage="Please select an activity!"
            CssClass="help-block">
        </asp:RequiredFieldValidator>
    </asp:Panel>

    <asp:Panel ID="ButtonsPanel" runat="server" CssClass="form-control-static">
        <!-- TODO Use resources to set text up in all the buttons -->
        <asp:Button ID="OK" runat="server"
            Text="<%# ButtonTexts.SwitchToSelectedActivity %>" CssClass="btn btn-primary btn-lg" Visible="false"
            OnClick="OK_Click" OnClientClick="return disableActivities()" />
        <asp:LinkButton runat="server" ID="SwitchNew" CssClass="btn btn-primary btn-lg" Text="<%# ButtonTexts.CreateActivity %>" Visible="false"
            PostBackUrl="~/SwitchNew.aspx" OnClientClick="return disableActivities()">
        </asp:LinkButton>
    </asp:Panel>
</asp:Content>
