﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Autofac;
using TimeSuite.SuiteLib.Globals;
using TimeSuite.SuiteLib.Interfaces.Repositories;
using TimeSuite.SuiteLib.Models;
using TimeSuite.TrackerApp.Pages;

namespace TimeSuite.TrackerAsp
{
    public partial class SwitchUnfinished : FormPage
    {
        protected override void OnPageLoad()
        {
            if (!IsPostBack && null == ActivitiesList.DataSource)
            {
                LoadUnfinishedActivities();
                VisualiseAppropriateControls();

                Page.DataBind();
            }
        }

        private void LoadUnfinishedActivities()
        {
            ActivitiesList.DataSource = GetUnfinishedActivitiesExceptCurrent();
        }

        private IEnumerable<Activity> GetUnfinishedActivitiesExceptCurrent()
        {
            var userID = 1;
            IEnumerable<Activity> activities;

            using(var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                var repo = scope.Resolve<IActivitiesRepository>();

                var currentActivity = repo.GetCurrent(userID);

                activities = repo.GetUnfinished(userID)
                    .Where(u => (null == currentActivity) || (currentActivity.ActivityID != u.ActivityID));
            }

            return activities;
        }

        private void VisualiseAppropriateControls()
        {
            var noUnfinishedActivities = 0 == ((ActivitiesList.DataSource as IEnumerable<Activity>)?.Count()).GetValueOrDefault(0);

            VisualizeAppropriatePanelControl(noUnfinishedActivities);
            VisualizeAppropriateButtonControl(noUnfinishedActivities);
        }

        private void VisualizeAppropriatePanelControl(bool noUnfinishedActivities)
        {
            (noUnfinishedActivities ? NoUnfinishedActivities : ThereAreUnfinishedActivities).Visible = true;
        }

        private void VisualizeAppropriateButtonControl(bool noUnfinishedActivities)
        {
            (noUnfinishedActivities ? SwitchNew : OK as WebControl).Visible = true;
        }

        protected void OK_Click(object sender, EventArgs e)
        {
            if(SwitchActivity())
                RedirectToDefaultDocument();
        }

        /// <returns>
        /// True if switch has been completed successfully and False if switching failed.
        /// </returns>
        private bool SwitchActivity()
        {
            // TODO: Raise an error if the unfinished activity was not elected by the user.

            if(!Page.IsValid)
                return false;

            var activityID = GetSelectedActivityID();

            if(!activityID.HasValue)
                return false;

            StopCurrentActitify();
            SwitchToUnfinishedActivity(activityID.Value);

            return true;
        }

        private int? GetSelectedActivityID()
        {
            return int.TryParse(ActivitiesList.SelectedValue, out var activityID) ? activityID : (int?)null;
        }

        private void StopCurrentActitify()
        {
            // TODO: Move this method from the current class and also from the SwitchNew.aspx.cs too to the `SwitchActivityFormPage:FormPage`.
            // TODO: Make the repository singleton using a DI container (in all the code files).
            using(var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                var repo = scope.Resolve<IActivitiesRepository>();
                int? currentActivityID = repo.GetCurrent(userID: 1)?.ActivityID;

                if (currentActivityID.HasValue)
                {
                    repo.StopOne(currentActivityID.Value);
                }
            }
        }

        private void SwitchToUnfinishedActivity(int activityID)
        {
            // TODO: Raise an error if `int.Parse(ActivitiesList.SelectedValue)` failed.
            // TODO: Lock the `Switch to it` button if the new activity caption is empty of unfinished activity is not picked.

            using(var scope = GlobalVariables.CompositionRoot.LifetimeScope)
            {
                scope.Resolve<IActivitiesRepository>().Resume(activityID);
            }
        }
    }
}